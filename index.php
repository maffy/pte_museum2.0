<?php
    //megvizsgáljuk hogy léteznek-e a működéshez szükséges fájlok
    if( file_exists( __DIR__."/config/config.php") && file_exists( __DIR__."/.htaccess") ) {
        //meghívjuk ezeket a fájlokat
		require ('config/config.php');
        require ('util/Auth.php');
		require ('util/Functions.php');
		require ('util/Picture.php');
		require ('util/Log.php');

		//létrehozzuk a képeknek a mappákat ha nem léteznének
        if( !file_exists(UPLOAD_PICT_DIR) ) mkdir(UPLOAD_PICT_DIR);
        if( !file_exists(WATER_PICTURES) ) mkdir(WATER_PICTURES);

		//betöltjük az osztályainkat
        function __autoload($class){
            require LIBS . $class. '.php';
        }

        //security
        $security = 1;

		//inicializáljuk a session-t
        Session::init();

		//meghívjuk a bootstrapet az oldal összeállításához
        $app = new Bootstrap();
    } else {
        echo "Nincs meg valamelyik kulcs fájl: '.htacces' vagy '/config/config.php'";
    }
?>