<?php

class Login_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function run(){
        $username = $_POST['username'];
        $password = $_POST['password'];

        $sth = $this->db->prepare("SELECT id FROM user WHERE username=:username AND password=:password");
        $sth->execute(array(
            ":username" => $username,
            ":password" => Hash::create('md5',$password)
        ));

        $id = $sth->fetch(PDO::FETCH_ASSOC);

        if( $sth->rowCount()>0){
            Session::init();
            Session::set('loggedIn',true);
            Session::set('role_id',$id);
            header('location: ' .URL.'admin');
        }else{
            header('location: ' .URL. 'login');
        }
    }

    public function logout(){
        Session::destroy();
        header('Location:'.URL);
    }
}