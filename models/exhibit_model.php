<?php

class Exhibit_Model extends Model {
    
    public function __construct(){
        parent::__construct();
        //karakterkódolási problémák megoldása a következő 2 sorral
        $this->db->exec("SET CHARACTER SET utf8");
        $this->db->exec("SET NAMES utf8");
    }
   
    /**
     * Visszaadja az összes elemet a kiállíásból
     * @return array() - fetchelt array az elemekről (minden oszlop)
     */
    public function getAllElements(){
        //lekérdezzük az összes kiállítás elemét és visszaadjuk
        $result = $this->db->select("SELECT * FROM exhibit");
        return $result;
    }
    
    /**
     * Kiválaszt az adott id alapján
     * @param integer $id
     */
    public function selectByID( $id ){
        $result = $this->db->select( "SELECT * FROM exhibit WHERE id=$id" );
        return $result[0];
    }
    
    /**
     * Kiválasztja a sorhoz tartozó segédtáblát
     * @param integer $id
     * @return array()
     */
    public function selectImTableByID( $id ){
        $result = $this->db->select( "SELECT * FROM exhibit_im__$id" );
        return $result;
    }
}