<?php

class User_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function issetUsername( $username ){
        $sth = $this->db->prepare( "SELECT * FROM user WHERE username=:username");
        $sth->execute(array(":username" => $username));
        return $sth->rowCount();
    }

    /** Visszaadja az adott felhasználó(k) adatait
     * @param $id
     * @return mixed
     */
    public function select( $table , $id=null ){
        if( $id!=null ) {
            $result = $this->db->select( "SELECT * FROM $table WHERE id=$id" );
            return $result[0];
        } else {
            $result = $this->db->select( "SELECT * FROM $table" );
            return $result;
        }
    }

    /**
     * A megfelelő kép altáblából a megfelelő képet törli
     * @param integer $table
     * @param integer $id
     */
    public function deleteByID( $table , $id ){
        $sth = $this->db->prepare("DELETE FROM $table WHERE id=$id");
        $sth->execute();
    }

    /**
     * Kiválaszt az adott id alapján az adott táblából
     * @param string $table
     * @param integer $id - Ha null akkor minden elemet kérünk
     * @return array()
     */
    public function selectByID( $table , $id=null ){
        if( $id!=null ) {
            $result = $this->db->select( "SELECT * FROM $table WHERE id=$id" );
            return $result[0];
        } else {
            $result = $this->db->select( "SELECT * FROM $table" );
            return $result;
        }
    }

    /**
     * Frissíti az adatokat
     * @param $data
     * @param $rank
     */
    public function update( $data , $rank ){
        $where = "id=".$data["id"];
        $this->db->updateDyn('user',$data,$where);
        $this->db->updateDyn('role',$rank,$where);
    }

    /**
     * Felviszi az adatokat a felhasználóról
     * @param $data
     * @param $rank
     */
    public function create( $data , $rank ){
        $this->db->insertDyn('user',$data);
        $sth = $this->db->prepare("SELECT id FROM user WHERE username=:username");
        $sth->execute(array(":username"=>$data["username"]));
        $id = $sth->fetch(PDO::FETCH_ASSOC);
        $rank["uid"] = $id["id"];
        $this->db->insertDyn('role',$rank);
    }

    public function delete( $id ){
        //megnézzük van-e jogosultság hozzá
        if( Session::get("rank")!=100 ) return false;

        //magasabb vagy azonos rank-al rendelkezőt ne törölhessünk
        $sth = $this->db->prepare("SELECT rank FROM user WHERE id=$id");
        $sth->execute();
        $data = $sth->fetch(PDO::FETCH_ASSOC);

        if( $data["rank"]<Session::get("rank") ) return false;

        //$id azonos�t�j� b�csi t�rl�se
        $sth = $this->db->prepare('DELETE FROM user WHERE id=$id');
        $sth->execute();
    }

    public function selectForMod( $id ){
        $sth = $this->db->prepare("SELECT * FROM user LEFT JOIN role ON user.id=role.uid WHERE user.id=:id");
        $sth->execute(array(":id"=>$id));
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
}