<?php

class Content_Model extends Model{

    public function construct(){
        parent::__construct();
    }

    /**
     * Lekéri adatbázisból a paraméterül kapott oldal tartalmát
     * @param $page - az oldal amihez tartozó tartalmat akarjuk megjeleníteni
     */

    public function getContent($page)
    {
        $statement = $this->db->prepare('SELECT title,content FROM content WHERE page = :page');
        $statement->execute(array(
            ':page' => $page
        ));

        return $statement->fetch();
    }

    public function update($content,$page){
        $where = 'page = "'.$page.'"';

        $this->db->updateDyn('content',$content,$where);
    }

    public function add($content){

        $title = $content['title'];
        $content = $content['content'];
        $page = 'home';
        $statement = $this->db->prepare("INSERT INTO content (page,title,content) VALUES('home','".$title."','".$content."')");

        $statement->execute();
    }

}