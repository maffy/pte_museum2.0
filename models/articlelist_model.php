<?php

class Articlelist_Model extends Model{

    public function construct(){
        parent::__construct();
    }

    /**
     * Meghívja a Database select metódusát
     * Article rekordokból álló tömböt ad vissza a select
     * @return array Array-t tartalmazó Array
     **/

    public function listArticle(){

        return $this->db->select('SELECT * FROM article');
    }

    /**
     * Egyadott azonosítójú article részletes adatait írja ki.
     * @param $id - az article azonosítója
     * @return mixed - a visszakapott article rekord
     */

    public function listDetails($id){

        $statement =$this->db->prepare('SELECT * FROM article WHERE idarticle = :idarticles');
        $statement->execute(array(
            ':idarticles' => $id));

        return $statement->fetch(PDO::FETCH_ASSOC);

    }

    /**
     * Egy adott articlehoz tartozó tag-eket keresi meg az article_tag táblából
     * @param $id - az article azonosítója
     * @return array -  a visszakapott tag-ek
     */

    public function getArticleTags($id){
        $statement = $this->db->prepare('SELECT tag FROM article_tag WHERE articleid = :articleid');
        $statement->execute(array(
            ':articleid' => $id
        ));
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Egy adott articlehoz tartozó szerzőket keresi meg az article_author táblából
     * @param $id - az article azonosítója
     * @return array -  a visszakapott szerzők
     */

    public function getArticleAuthors($id){
        $statement = $this->db->prepare('SELECT author FROM article_author WHERE articleid = :articleid');
        $statement->execute(array(
            ':articleid' => $id
        ));
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Tag alapján keres cikkeket
     * Inner Join-nal összekapcsolja az article és az article_tag táblákat,
     * és azokat az article rekordokat adja vissza, ahol az article_tag.tag megfelel a kereesésnek
     * @param $tag a keresett tag
     */

    public function searchByTag($tag)
    {
        $sql = 'SELECT * FROM article INNER JOIN article_tag ON article.idarticle = article_tag.articleid
                WHERE article_tag.tag = :tag';
        $statement = $this->db->prepare($sql);
        $statement->execute(array(
            ':tag' => $tag
        ));
        return $statement->fetchAll();
    }

    /**
     * Szerző alapján keres cikkeket
     * Inner Join-nal összekapcsolja az article és az article_author táblákat,
     * és azokat az article rekordokat adja vissza, ahol az article_author.author megfelel a keresett Mintának
     * @param $author a keresett szerző
     */

    public function searchByAuthor($author){

        $sql = 'SELECT * FROM article INNER JOIN article_author ON article.idarticle = article_author.articleid
                WHERE article_author.author LIKE :author';
        $statement = $this->db->prepare($sql);
        $statement->execute(array(
            ':author' => '%'.$author.'%'
        ));

        return $statement->fetchAll();
    }

    public function searchAuthor(){

    }

    /*
    public function listArticleTag(){

        $sql = 'SELECT article.title, article.abstract, article_tag.tag FROM article LEFT JOIN article_tag
                ON article.idarticle = article_tag.articleid';
        $statement = $this->db->prepare($sql);
        $statement->execute();

        return $statement->fetchAll();
    }*/

}