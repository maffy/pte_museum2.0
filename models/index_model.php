<?php

class Index_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function loadContent(){

        $statement = $this->db->prepare('SELECT title,content FROM content WHERE page = :page');
        $statement->execute(array(':page' => 'home'));
        $statement->execute();

        return $statement->fetch();
    }


}