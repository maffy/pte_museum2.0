<?php

class Watermark_Model extends Model {
    public $model;

    public function __construct(){
        parent::__construct();

        //karakterkódolási problémák megoldása a következő 2 sorral
        $this->db->exec("SET CHARACTER SET utf8");
        $this->db->exec("SET NAMES utf8");
    }

    /**
     * Kiválaszt az adott id alapján az adott táblából
     * @param string $table
     * @param integer $id - Ha null akkor minden elemet kérünk
     * @return array()
     */
    public function selectByID( $table , $id=null ){
        if( $id!=null ) {
            $result = $this->db->select( "SELECT * FROM $table WHERE id=$id" );
            return $result[0];
        } else {
            $result = $this->db->select( "SELECT * FROM $table" );
            return $result;
        }
    }

    /**
     * Létrehoz egy sort az adatbázisban
     * @param string $table
     * @param array() $data
     */
    public function insert( $table , $data ){
        $this->db->insertDyn($table,$data);
    }

    /**
     * A megfelelő kép altáblából a megfelelő képet törli
     * @param integer $table
     * @param integer $id
     */
    public function deleteByID( $table , $id ){
        $sth = $this->db->prepare("DELETE FROM $table WHERE id=:id");
        $sth->execute(array(":id"=>$id));
    }
}