<?php

class Museum_Model extends Model {
    
    public function __construct(){
        parent::__construct();

        //karakterkódolási problémák megoldása a következő 2 sorral
        $this->db->exec("SET CHARACTER SET utf8");
        $this->db->exec("SET NAMES utf8");
    }

    /** Törli a megadott markot
     * @param $mark
     */
    public function delMark( $value ){
        $value = explode(".",$value);
        $value = $value[0];
        $sth = $this->db->prepare("DELETE FROM mark WHERE code=:code");
        $sth->execute( array(":code"=>$value) );
        //print_r($sth->errorInfo());
    }

    /**
     * Létrehoz egy sort az adatbázisban
     * @param string $table
     * @param array() $data
     */
    public function insert( $table , $data ){
        $this->db->insertDyn($table,$data);
    }

    /**
     * Visszaadja az összes elemet a táblából
     * @param string $table
     * @return array() - fetchelt array az elemekről (minden oszlop)
     */
    public function selectAll( $table ){
        //lekérdezzük az összes kiállítás elemét és visszaadjuk
        $result = $this->db->select("SELECT * FROM $table");
        return $result;
    }

    /**
     * Kiválaszt az adott id alapján az adott táblából
     * @param string $table
     * @param integer $id - Ha null akkor minden elemet kérünk
     * @return array()
     */
    public function selectByID( $table , $id=null ){
        if( $id!=null ) {
            $result = $this->db->select( "SELECT * FROM $table WHERE id=$id" );
            return $result[0];
        } else {
            $result = $this->db->select( "SELECT * FROM $table" );
            return $result;
        }
    }

    /**
     * A megfelelő kép altáblából a megfelelő képet törli
     * @param integer $table
     * @param integer $id
     */
    public function deleteByID( $table , $id ){
        $sth = $this->db->prepare("DELETE FROM $table WHERE id=$id");
        $sth->execute();
    }
    
    /**
     * Eldobja a megadott nevű táblát
     * @param string $table
     */
    public function dropTable( $table ){
        $sth = $this->db->prepare("DROP TABLE $table");
        $sth->execute();
    }

    /**
     * Fríssiti az id sorában az oszlopok tartalmát
     */    
    public function updateByID($table,$data,$where){
        $this->db->updateDyn($table,$data,$where);
    }
    
    /**
     * Létrehoz egy táblát a képeknek
     */
    public function imTableCreate( $id ){
        //Előkészítjük a statment-et
        $sth = $this->db->prepare("CREATE TABLE IF NOT EXISTS `exhibit_im__".$id."` (
            `id` int(11) NOT NULL auto_increment,
            `file` text collate utf8_hungarian_ci NOT NULL,
            `mark` text collate utf8_hungarian_ci NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=1 ;");
        
        //Statement futtatása
        $sth->execute();
    }
    
    public function getIdByMark( $t_code ){
        $sth = $this->db->prepare("SELECT id FROM exhibit WHERE mark='$t_code'");
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC)["id"];
    }
}