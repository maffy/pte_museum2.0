<?php
/**
 * Created by PhpStorm.
 * User: Maf
 * Date: 02/11/2015
 * Time: 12:59
 */

class Article_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    /**
     * Article Controller addByUrl($data) metódusa hívja
     * Hozzáad egy cikket az adatbázisbhoz
     * @param array $data az adatt�mb, FORM-r�l �rkez� adatokkal. kulcsok = t�blanevek
     */

    public function addArticleByUrl($data = array(),$tags = array()){
        //Ha nem voltak tag-ek megadva
        if(empty($tags)){
            $this->db->insertDyn('article',$data);
        }else{
            $this->db->insertArticleWithTags($data,$tags);
        }
    }

    public function listArticleDetails($id){

        //TODO menőbb függvényt írni DATABASE osztályban, úgy, hogy működjön dinamikusan listSingleUSerre is

        $statement =$this->db->prepare('SELECT * FROM article WHERE idarticle = :idarticles');
        $statement->execute(array(
                            ':idarticles' => $id));

        return $statement->fetch();

    }

    public function articleDetailsShort($id){

        /*        $statement =$this->db->prepare('SELECT * FROM article WHERE idarticle = :idarticles');
                $statement->execute(array(
                    ':idarticles' => $id));

                $result = $statement->fetch(PDO::FETCH_ASSOC); //indexelt tömbként adja vissza*/

        $sql = 'SELECT * FROM article WHERE idarticle = :idarticle';
        $params = array('idarticle' => $id);
        $result = $this->db->selectSingle($sql,$params);

        foreach($result as $key => $value){
            if(is_string($value)){
                if(strlen($value) > 50){
                    $result[$key] = substr($value,0,50);
                }
            }
        }
        return $result;
    }

    public function editSave($data = array(),$id = null){

        $where = 'idarticle = '.$id;
        $this->db->updateDyn('article',$data,$where);
        header('Location: '.URL.'article/manage');
    }

    public function delete($id){
        $sql = 'DELETE from article WHERE idarticle='.$id;
        $this->db->delete($sql);
    }
    public function deleteTag($id){
        $sql = 'DELETE FROM article_tag WHERE articleid='.$id;
        $this->db->delete($sql);
    }
    public function deleteAuthor($id){
        $sql = 'DELETE FROM article_author WHERE articleid='.$id;
        $this->db->delete($sql);
    }
    public function deleteFile($id){

        $sql = 'SELECT filelocation FROM article WHERE idarticle = :id';
        $statement = $this->db->prepare($sql);

        $statement->execute(array(':id' => $id));
        $filelocation = $statement->fetch();

        $filename = $filelocation['filelocation'];
        if(!empty($filename)){
            unlink($filename);
        }
    }

    /**
     * Meghívja a Database select metódusát
     * Article rekordokból álló tömböt ad vissza a select
     * @return array Array-t tartalmazó Array
    */

    public function listArticle(){

        return $this->db->select('SELECT * FROM article');
    }

    public function listArticleShort(){

        $statement = $this->db->prepare('SELECT idarticle,title,abstract,source,year FROM article');
        $statement->execute();

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        for($i=0;$i<count($result);$i++){

            $article = $result[$i];
            foreach($article as $key => $value){
                if(strlen($value) > 30)
                    $result[$i][$key] = substr($value,0,30) . '...';
            }
        }
        return $result;
    }

    public function addArticle($article,$tags,$authors){

        $this->db->addArticle($article,$tags,$authors);
    }

    public function getArticleAuthors($id){
        $statement = $this->db->prepare('SELECT author FROM article_author WHERE articleid = :articleid');
        $statement->execute(array(
            ':articleid' => $id
        ));
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

}
