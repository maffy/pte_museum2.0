<?php

class Login extends Controller{
    /**
     * @var Login_Model
     */
    public $model;

    function  __construct(){
        parent::__construct();
    }

    function index(){
        //ha már bevagyunk jelentkezve
        if (Session::get("loggedIn")==false){
            $this->view->render("login");
        }else {
            header('location: ' .URL);
        }
    }

    function run(){
        $this->model->run();
    }

    function logout(){
        $this->model->logout();
    }
}