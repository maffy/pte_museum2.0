<?php

class Admin extends Controller {
    /**
     * @var Admin_Model
     */
    public $model;

    function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }

    /**
     * Csak egy almenü a navigációhoz, hogy feleslegesen ne jelenjen meg semmi
     */
    function index(){
        $this->view->title = 'Admin - Index';
        $this->view->renderAdmin();
    }
}
