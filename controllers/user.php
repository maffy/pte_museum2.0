<?php

class User extends Controller{
    /**
     * @var User_Model
     */
    public $model;

    function  __construct(){
        parent::__construct();
        Auth::handleLogin();
        Auth::role("admAcs");
    }

    function index(){
        header("Location: ".URL."user/userlist");
    }

    function userlist(){
        $this->view->title = "Felhasználók";
        $this->view->userList = $this->model->select("user");
        $this->view->renderAdmin('admin/m_user');
    }

    function createuser(){
        $this->view->title = "Új felhasználó";
        $this->view->renderAdmin("admin/m_newuser");
    }

    public function create(){
        if( isset($_POST["username"]) && isset($_POST["password"]) ){
            unset($regResult);
            $regResult = "";

            //megnézzük hogy a felhasználónév foglalt-e
            $user = $this->model->issetUsername($_POST["username"]);

            if( $_POST["username"]=="" )				     $regResult="Üres username mező!";
            else if( $user>0 )                                 $regResult="Ez a felhasználónév foglalt!";
            else if( $_POST["realname"]=="")				 $regResult="Üres realname mező!";
            else if( $_POST["password"]=="")				 $regResult="Üres jelszó mező!";
            else if( $_POST["password"]!=$_POST["passconf"] )$regResult="A két jelszó mező különbözik!";
            else if( $_POST["email"]=="")				     $regResult="Üres e-mail mező!";
            else if( mb_strlen($_POST["password"])<5 ) 	     $regResult="Túl rövid a jelszó! (min 5 karakter lehet)";
            else {
                //$data tömb feltöltése az ürlapról érkező adatokkal
                $data = array();
                $rank = array();
                $data['username'] = $_POST['username'];
                $data['password'] = Hash::create('md5',$_POST['password']);
                $data["realname"] = $_POST["realname"];
                $data['email'] = $_POST['email'];
                $data['regip'] = $_SERVER['REMOTE_ADDR'];
                $data['regtime'] = date('Y-m-d');
                $data['lastmodify'] = date('Y-m-d');
                $data['lastin'] = date('Y-m-d');
                $data['ban'] = 0;
                $rank['exhAcs'] = isset($_POST["exhAcs"])?$_POST["exhAcs"]:0;
                $rank['exhMod'] = isset($_POST["exhMod"])?$_POST["exhMod"]:0;
                $rank['exhDel'] = isset($_POST["exhDel"])?$_POST["exhDel"]:0;
                $rank['elmAcs'] = isset($_POST["elmAcs"])?$_POST["elmAcs"]:0;
                $rank['elmUpl'] = isset($_POST["elmUpl"])?$_POST["elmUpl"]:0;
                $rank['elmWat'] = isset($_POST["elmWat"])?$_POST["elmWat"]:0;
                $rank['elmOwn'] = isset($_POST["elmOwn"])?$_POST["elmOwn"]:0;
                $rank['watAcs'] = isset($_POST["watAcs"])?$_POST["watAcs"]:0;
                $rank['watUpl'] = isset($_POST["watUpl"])?$_POST["watUpl"]:0;
                $rank['watDel'] = isset($_POST["watDel"])?$_POST["watDel"]:0;
                $rank['admAcs'] = isset($_POST["admAcs"])?$_POST["admAcs"]:0;
                $rank['admMod'] = isset($_POST["admMod"])?$_POST["admMod"]:0;
                $rank['admDel'] = isset($_POST["admDel"])?$_POST["admDel"]:0;

                //create method meghívása $data tömbbel és oldal frissítése
                $this->model->create($data,$rank);
                $this->view->srvMsg = array();
                $this->view->srvMsg[0]["message"] = "Regisztáció sikeres!";
                $this->view->srvMsg[0]["success"] = 1;
                $this->view->srvNxt = "user/createuser";
                $this->view->renderAdmin("admin/m_answer");
            }
            if( $regResult!="" ){
                $this->view->srvMsg = array();
                $this->view->srvMsg[0]["message"] = "<p>".$regResult."</p>";
                $this->view->srvMsg[0]["success"] = 0;
                $this->view->srvNxt = "user/createuser";
                $this->view->renderAdmin("admin/m_answer");
            }
        } else {
            $this->view->renderAdmin("error/index");
        }
    }

    public function delete($id){
        Auth::role("admDel");
        Session::set("act","preDelete");

        //id alapján kiszedjük az item sorát
        $this->view->exhElem = $this->model->selectByID( "user" , $id );
        $this->view->cancel = "user/userlist";
        $this->view->accept = "user/doDelete/";

        //megjelenítjük a megerősítőlapot
        $this->view->renderAdmin('admin/m_delete');
    }

    public function doDelete($id){
        if( Session::get("act")=="preDelete" ) {
            $this->view->srvMsg = array();
            $this->view->srvMsg[0]["success"] = 1;
            $this->view->srvMsg[0]["message"] = "Sikeresen törölt elem!";
            $this->view->srvNxt = "user/userlist";

            //A főtáblában töröljük a sort
            $this->model->deleteByID("user", $id);

            $this->view->renderAdmin("admin/m_answer");
            Session::set("act","none");
        } else {
            $this->view->renderAdmin("error/index");
        }
    }

    public function edit($id){
        Auth::role("admMod");
        Session::set("pre_edit",1);
        $this->view->data["id"] = $id;
        $this->view->data = $this->model->selectForMod($id);
        $this->view->render('admin/m_edituser');
    }

    public function modify($id){
        if( isset($_POST) && Session::get("pre_edit")==1 ){
            unset($regResult);
            $regResult = "";

            if( $_POST["email"]=="") $regResult="Üres e-mail mező!";
            else {
                $data = array();
                $rank = array();
                $data['id'] = $id;

                $data["realname"] = $_POST["realname"];
                $data['email'] = $_POST['email'];
                $data['regip'] = $_SERVER['REMOTE_ADDR'];
                $data['lastmodify'] = date('Y-m-d');
                $rank['exhAcs'] = isset($_POST["exhAcs"])?$_POST["exhAcs"]:0;
                $rank['exhMod'] = isset($_POST["exhMod"])?$_POST["exhMod"]:0;
                $rank['exhDel'] = isset($_POST["exhDel"])?$_POST["exhDel"]:0;
                $rank['elmAcs'] = isset($_POST["elmAcs"])?$_POST["elmAcs"]:0;
                $rank['elmUpl'] = isset($_POST["elmUpl"])?$_POST["elmUpl"]:0;
                $rank['elmWat'] = isset($_POST["elmWat"])?$_POST["elmWat"]:0;
                $rank['elmOwn'] = isset($_POST["elmOwn"])?$_POST["elmOwn"]:0;
                $rank['watAcs'] = isset($_POST["watAcs"])?$_POST["watAcs"]:0;
                $rank['watUpl'] = isset($_POST["watUpl"])?$_POST["watUpl"]:0;
                $rank['watDel'] = isset($_POST["watDel"])?$_POST["watDel"]:0;
                $rank['admAcs'] = isset($_POST["admAcs"])?$_POST["admAcs"]:0;
                $rank['admMod'] = isset($_POST["admMod"])?$_POST["admMod"]:0;
                $rank['admDel'] = isset($_POST["admDel"])?$_POST["admDel"]:0;

                $this->model->update($data,$rank);
                $this->view->srvMsg = array();
                $this->view->srvMsg[0]["message"] = "Felhasználó frissítése sikeres!";
                $this->view->srvMsg[0]["success"] = 1;
                $this->view->srvNxt = "user/userlist";
                $this->view->renderAdmin("admin/m_answer");
            }
            if( $regResult!="" ){
                $this->view->srvMsg = array();
                $this->view->srvMsg[0]["message"] = "<p>".$regResult."</p>";
                $this->view->srvMsg[0]["success"] = 0;
                $this->view->srvNxt = "user/userlist";
                $this->view->renderAdmin("admin/m_answer");
            }

            unset($_POST);
            Session::set("pre_edit",0);
        } else {
            $this->view->renderAdmin("error/index");
        }
    }
}

?>