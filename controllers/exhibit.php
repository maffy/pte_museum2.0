<?php

class Exhibit extends Controller {
    /**
     * @var Exhibit_Model
     */
    public $model;

    function __construct() {
        parent::__construct();
    }

    /**
     * Tárlat megjelenítése mindenki számára
     */
    function index(){
        $this->view->title = 'Múzeum tárlat';
        
        //lekérjük a tárlat elemeit
        $elem = $this->model->getAllElements();
        
        //hozzárendeljük a sorhoz tartozó képeket
        for( $i=0 ; $i<count($elem) ; $i++ ){
            $elem[$i]["im"] = $this->model->selectImTableByID($elem[$i]["id"]);
        }

        //view osztálynak átadás
        $this->view->exhElem = $elem;
        $this->view->render('museum/view');
    }
    
    /**
     * Adott elem megtekintése a tárlatban
     * @param integer $id - Melyik sorhoz tartozik
     */
    function view( $id ){
        //akarunk slide-ot? - 0 képnél nem nagyon
        $this->view->slide = true;
        $this->view->inbig = false;

        //kiválasztjuk a képek tábláját
        $exhIm = $this->model->selectImTableById( $id );

        //megnézzük hogy kell-e slide-ot csinálnunk
        if( count($exhIm)==0 || count($exhIm)==1 ) $this->view->slide = false;

        //továbbadjuk a képeink
        $this->view->pic = array();
        for( $i=0 ; $i<count($exhIm) ; $i++ ){
            $this->view->pic[$i] = URL.UPLOAD_PICT_DIR.(isset($exhIm[$i]["mark"])?$exhIm[$i]["mark"]:"");
        }

        //válaszzuk ki a sorban a címet, tárgyat, szerzőt stb... adatokat
        $this->view->exhElem = $this->model->selectByID( $id );

        //megjelenítés
        $this->view->render('museum/elem');
    }
}