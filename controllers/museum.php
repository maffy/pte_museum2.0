<?php

class Museum extends Controller {
    /**
     * @var Museum_Model
     */
    public $model;

    function __construct() {
        parent::__construct();
        Auth::handleLogin();
        Auth::role("exhAcs");
    }

    function index(){
        $this->view->render("error/index");
    }

    /**
     * Tárlat megjelenítés, elemek felsorolása
     */
    function exhibition(){
        $this->view->title = 'Admin - Tárlat';
        $this->view->listElem = $this->model->selectAll( "exhibit" );
        $this->view->renderAdmin('admin/m_exhibition');
    }

    /**
     * Hozzáadás lap megjelenítés
     */
    function newitem(){
        Auth::role("elmAcs");
        $this->view->title = 'Admin - Hozzáadás';
        $this->view->listElem = $this->model->selectAll( "exhibit" );
        $this->view->listWater = $this->model->selectAll( "water" );
        $this->view->renderAdmin('admin/m_newitem');
    }

    /**
     * Módosítani lehet a felvitt múzeumi dolgok paramétereit
     * @param integer $id
     */
    function edit( $id ){
        Auth::role("exhMod");
        Session::set("act","preEdit");
        $this->view->title = 'Amdin - Szerkesztés';

        $this->view->listElem = $this->model->selectAll( "exhibit" );
        $this->view->listWater = $this->model->selectAll( "water" );

        //id alapján kiszedjük az item sorát és továbbadjuk megjelenítésre
        $this->view->itemDetail = $this->model->selectByID( "exhibit" , $id );
        $this->view->exhIm = $this->model->selectByID( "exhibit_im__".$id );

        //megjelenítjük a módosítás lapját
        $this->view->renderAdmin('admin/m_edit');
    }

    /**
     * Megadott módosítások elvégzése
     * @param integer $id
     */
    function doEdit( $id ){
        if( Session::get("act")=="preEdit" ){
            $this->view->srvMsg = array(array());
            $this->view->srvMsg[0]["success"] = 1;
            $this->view->srvMsg[0]["message"] = "Sikeresen módosított elem!";
            $this->view->srvNxt = "museum/exhibition";

            //megváltoztatjuk az elküldött adatokat
            $data = array();
            $data["title"] = trim(Functions::filter($_POST["title"]));
            $data["auth"] = trim(Functions::filter($_POST["auth"]));
            $data["disc"] = trim($_POST["disc"]);
            $data["modified"] = date('Y-m-d');
            $this->model->updateByID('exhibit',$data,'id='.$id);

            //kezeljük a képeket
            $this->view->srvMsg = Picture::handle( $id );

            $this->view->renderAdmin("admin/m_answer");
            Session::set("act","none");
        } else {
            $this->view->renderAdmin("error/index");
        }
    }

    /**
     * Törlés
     * @param integer $id
     */
    function delete( $id ){
        Auth::role("exhDel");
        //állítsuk be biztonságos törléshez
        Session::set("act","preDelete");

        //id alapján kiszedjük az item sorát
        $this->view->exhElem = $this->model->selectByID( "exhibit" , $id );
        $this->view->cancel = "museum/exhibition";
        $this->view->accept = "museum/doDelete/";

        //megjelenítjük a megerősítőlapot
        $this->view->renderAdmin('admin/m_delete');
    }

    /**
     * Biztonsági kérdés a törléshez
     * @param integer $id
     */
    function doDelete( $id ){
        if( Session::get("act")=="preDelete" ){
            //szerver válasz előkészítése
            $this->view->srvMsg = array();
            $this->view->srvMsg[0]["success"] = 1;
            $this->view->srvMsg[0]["message"] = "Sikeresen törölt elem!";
            $this->view->srvNxt = "museum/exhibition";

            //kiválasztjuk a táblában lévő képek neveit a törlés előkészítéséhez
            $item = $this->model->selectByID( "exhibit_im__".$id );

            //eldobjuk a képek táblát
            $this->model->dropTable('exhibit_im__'.$id);

            //fájlok törlése
            for( $i=0 ; $i<count($item) ; $i++ ){
                //fájlok törlése
                if( file_exists(UPLOAD_PICT_DIR.$item[$i]["file"]) ){ unlink( UPLOAD_PICT_DIR.$item[$i]["file"] );}
                if( file_exists(UPLOAD_PICT_DIR.$item[$i]["mark"]) ) {unlink( UPLOAD_PICT_DIR.$item[$i]["mark"] );}
                $this->model->delMark($item[$i]["mark"]);
            }

            //Végül a főtáblában is töröljük a sort
            $this->model->deleteByID( "exhibit" , $id );

            $this->view->renderAdmin("admin/m_answer");
        } else {
            $this->view->renderAdmin("error/index");
        }
    }

    /** Elavult metódus TODO: ellenörzés és eltávolítás
     * A megfelelő altáblából kitöröljük a képet
     * @param integer $mainId
     * @param integer $imId
     */
    function doDelPic( $mainId , $imId ){
        //lekérdezzük a táblában lévő
        $data = $this->model->selectByID( "exhibit_im__".$mainId , $imId );
        $this->model->deleteByID( "exhibit_im__".$mainId , $imId );

        //fájlok törlése
        unlink( UPLOAD_PICT_DIR.$data["file"] );
        unlink( UPLOAD_PICT_DIR.$data["mark"] );

        //ez nem szép átirányítás
        header("Location:".URL."museum/edit/".$mainId);
    }

    /**
     * Mivel a tárlat listában nem látjuk a képeket és az infók nagy részét is csak rövidítve, ezért egyesével is lehet nézni őket
     * @param integer $id
     */
    function check( $id ){
        $this->view->title = 'Admin - Több';
        $this->view->exhIm = $this->model->selectByID( "exhibit_im__".$id );
        $this->view->exhElem = $this->model->selectByID( "exhibit" , $id );
        $this->view->renderAdmin('admin/m_check');
    }

    /**
     * Hozzáad egy elemet a kiállításhoz
     * Minden adat a $_POST[]-ban van
     */
    function add(){
        Auth::role("elmUpl");
		//TODO: javascript ellenörzés JavaPost::valideate();
		//TODO: rollback Rollback::check();
        //szerver válasz előkészítése
        $this->view->srvMsg = array();
        $this->view->srvNxt = "museum/newitem";

        //kód hogy visszakérhessük az ID-t
        $t_code = Functions::getRCG();

        //hozzuk létre a sort a táblában
        $data = array();
        $data["title"] = trim(Functions::filter($_POST["title"]));
        $data["auth"] = trim(Functions::filter($_POST["auth"]));
        $data["mark"] = trim($t_code);
        $data["disc"] = trim($_POST["disc"]);
        $data["date"] = date('Y-m-d');
        $data["modified"] = date('Y-m-d');
		$this->model->insert('exhibit',$data);

        //kérjük vissza a létrehozott sor id-jét
        $id = $this->model->getIdByMark( $t_code );

        //hozzuk létre a hozzátartozó táblát amely tartalmazza majd a képeket és a watermark-os változatuk
        $this->model->imTableCreate( $id );

        //kezeljük a képeket
        $this->view->srvMsg = Picture::handle( $id );

        //hívjuk meg az admin válaszát
        $this->view->renderAdmin("admin/m_answer");
    }
}
