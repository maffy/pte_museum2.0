<?php
/**
 * Created by PhpStorm.
 * User: Maf
 * Date: 16/11/2015
 * Time: 12:39
 */

class Articlelist extends Controller{

    /**
     * @var Articlelist_Model
     */
    public $model;

    /**
     * @var View
     */
    public $view;

    public function __construct(){
        parent::__construct();
    }

   public function index(){
        $this->view->articleList = $this->model->listArticle();
        $this->view->title = 'Publikációk';
        $this->view->render('articlelist/index');
    }

    public function details($id){

        $this->view->articleDetails = $this->model->listDetails($id);
        $this->view->articleTags = $this->model->getArticleTags($id);
        $this->view->articleAuthors = $this->model->getArticleAuthors($id);
        $this->view->title = $this->view->articleDetails['title'];
        $this->view->render('articlelist/details');
    }

    public function searchByTag($tag){
        $this->view->articleList = $this->model->searchByTag($tag);
        $this->view->title = $tag;
        $this->view->render('articlelist/index');
    }

    public function searchByAuthor($author){

        $this->view->articleList = $this->model->searchByAuthor($author);
        $this->view->title = $author;
        $this->view->render('articlelist/index');
    }

    public function search(){

        if(isset($_POST['search'])){

            if($_POST['searchby'] == 'tag'){

                $tag = Functions::filter($_POST['text']);
                $this->searchByTag($tag);
            }else {

                $author = Functions::filter($_POST['text']);
                $author = str_replace('', '_', $author);
                $this->searchByAuthor($author);

            }
            unset($_POST);
            return false;
        }else{
            header('location:'.URL.'articlelist');
            return false;
        }
    }

    /*    public function searchAuthor(){
        if(isset($_POST['searchAuthor'])){

            $author = Functions::filter($_POST['author']);
            $author = str_replace('','_',$author);
            unset($_POST);
            $this->searchByAuthor($author);
            return false;
        }else{
            return false;
        }
    }

    public function searchTag(){
        if(isset($_POST['searchTag'])){

            $tag = Functions::filter($_POST['tag']);

            $this->searchByTag($tag);
            return false;
        }else{
            return false;
        }
    }*/
}