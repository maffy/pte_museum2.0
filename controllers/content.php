<?php

class Content extends Controller{

    /**
     * @var Content_Model
     */
    public $model;

    public function __construct(){
        parent::__construct();
        Auth::handleLogin();
    }

    public function index(){
        $this->view->title = 'Error';
        $this->view->render('error/index');
    }

    /**
     * Tartalom kezelő oldal a kezdőlaphoz - View meghívása
     */
    public function manage(){

        $this->view->contentHome = $this->model->getContent('home');
        $this->view->contentContact = $this->model->getContent('contact');
        $this->view->renderAdmin('content/manage');

    }

    public function update($page){

        if(isset($_POST['update'])){

            $content['title'] = $_POST['title'];
            $content['content'] = $_POST['content'];

            $this->model->update($content,$page);
            $this->view->msg = 'Sikeres módosítás';
            $this->view->renderAdmin('article/answer');
        }else{
            header('Location:'.URL);
            return false;
        }
    }

    public function add(){

        if(isset($_POST['update'])){

            $content['title'] = $_POST['title'];
            $content['content'] = $_POST['content'];

            $this->model->add($content);

        }
    }
}