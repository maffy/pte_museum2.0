<?php

class Contact extends Controller{

    public function  __construct(){
        parent::__construct();
    }

    public function index(){
        $this->view->content = $this->model->loadContent();
        $this->view->render('contact/index');
    }
}

?>