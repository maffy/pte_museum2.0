<?php


class Article extends Controller
{
    /**
     * @var Article_Model
     */
    public $model;

    public function __construct()
    {
        parent::__construct();
        Auth::handleLogin();
    }

    public function index()
    {

        $this->view->articleList = $this->model->listArticle();
        $this->view->render('articlelist/index');
    }
    //Publikáció hozzáadása - View meghívása

    public function create()
    {
        $this->view->renderAdmin('article/new');
    }

    //Publikáció hozzáadása fileból - View meghívása
    public function upload()
    {
        $this->view->renderAdmin('article/upload');
    }

    //Pubklikáció kezelő oldal - View meghívása
    public function manage()
    {
        $this->view->articleList = $this->model->listArticleShort();
        $this->view->renderAdmin('article/manage');
    }

    /**
     * Adott azonosítójú article részleteinek listázása
     * Meghívjuk a model listArticleDetails függvényét
     * articleDetails - dinamikusan deklarálva. Minden adat ebbe
     * @param $id idarticle - a publikáció azonosítója
     */
    public function details($id)
    {
        $this->view->articleDetails = $this->model->listArticleDetails($id);
        $this->view->render('article/details');
    }

    /**
     * Publikáció hozzáadása
     */

    public function add(){

        if(!isset($_POST['add'])){

            header('Location:'.URL.'article/create');
            return false;
        }else{

            $errorOccurred = false; //Hibák jelzésére

            //Mezők amelyek nem lehetnek üresek
            $article['title'] = $_POST['title'];
            $article['abstract'] = $_POST['abstract'];

            //Kötelező mezők ellenőrzése
            foreach ($article as $key => $value) {
                if (strlen($value) < 1) {
                    $errorOccurred = true;
                    $this->view->errorMsg[$key] = 'A ' . $key . ' mező nem lehet üres ';
                }
            }
            //Adatok tömbbe másolása
            $article['content'] = $_POST['content'];
            $article['location'] = $_POST['location'];
            $article['year'] = $_POST['year'];
            $article['source'] = (strlen($_POST['source']) > 1 ? $_POST['source'] : 'Ismeretlen') ;

            //Ha volt hiba, adatok visszaírása dataBack tömbbe, és view újrahívása
            if($errorOccurred){
                foreach($article as $key => $value){
                    $this->view->dataBack[$key] = $value;
                }
                $this->view->dataBack['tags'] = $_POST['tags'];
                $this->view->dataBack['authors'] = $_POST['authors'];
                $this->view->renderAdmin('article/new');
                return false;
            }else{
                //Ha kiválasztottunk egy fájlt, megkíséreljük a feltöltését
                if(isset($_FILES['file'])){
                    $fileData = $_FILES['file'];
                    //Sikeres feltöltés, adatbázisba írás
                    if($this->addFile($fileData)){
                        $article['filelocation'] = $this->fileDest;    //Feltöltöss fájl elérési útja
                        $article['isFile'] = 1; //Fájmutató - van hozzárendelve fájl
                    }
                    //Hiba a fájlfeltöltés során
                    else{
                        $this->view->errorMsg = $this->fileErrorMsg;
                        //TODO: Prevent, hogy adatbázisba írjunk
                    }
                }else{
                    $article['isFile'] = 0;     //Fájlmutató
                    $article['fileLocation'] = '';
                }

                $article['dateAdded'] = date('Y-m-d');

                $tags = array();
                $authors  = array();

                //Tage-ek tömbbe másolása
                $_POST['tags'] = rtrim($_POST['tags'], ",");     //Végéről vessző levágása, ha van
                $tags = explode(',', trim($_POST['tags']));      //vessző alapján felbontás

                //Ha üres a szerző mező, ismeretlen szerző
                if(empty($_POST['authors'])){
                    $authors[0] = 'Ismeretlen szerzo';
                }else{
                    $_POST['authors'] = rtrim($_POST['authors'],",");   //Végéről vessző levágása, ha van
                    $authors = explode(',', trim($_POST['authors']));   //vessző alapján felbontás
                }

                $this->model->addArticle($article,$tags,$authors);
                $this->view->msg = 'Sikeres feltöltés';
                $this->view->renderAdmin('article/answer');
            }
        }
    }

    public function addFile($file){

        if(empty($file)){
            $this->fileErrorMsg = 'nincs adat';
            return false;
        }else{
            $temp = $file['tmp_name'];

            $allowed_extensions = array('pdf','doc','docx','txt');
            $sizelimit = 500000000;

            //Kiterjesztés megvizsgálása
            $fileOrg = $file['name'];
            $fileOrg = explode('.', $fileOrg);
            $fileName = strtolower($fileOrg[0]);
            $file_ext = strtolower(end($fileOrg));

            if(!in_array($file_ext,$allowed_extensions)){
                $this->fileErrorMsg = 'Nem megengedett kiterjesztés';
                return false;
            }
            if($file['size'] > $sizelimit){
                $this->fileErrorMsg = 'Túl nagy fájl';
                return false;
            }
            //Egyéni fájlnév generálása TODO : config fájlba az alapértelmezett útvonalat
            //$file_name = uniqid('', true) . '.' . $file_ext;
            $file_name = $fileName . '.' . $file_ext;

            $destination = 'public/data/' . $file_name;

            if (move_uploaded_file($temp, $destination)) {
                $this->fileDest = $destination;
                return true;

            } else { //Hiba a fájlfeltöltés során
                $errorOccurred = true;
                $this->fileErrorMsg = 'Valami hiba tortent a feltoltes soran';
                return false;
            }
        }
    }

    /**
     * Meghívja szerkesztésre az $id azonosítójú pubkliációt
     * Publikációk adatai-val meghívja a publikáció szerkesztés lapot
     * @param $id publikáció azonosítója
*/

    public function edit($id)
    {
        $this->view->articleDetails = $this->model->listArticleDetails($id);
        $this->view->authors = $this->model->getArticleAuthors($id);
        $this->view->render('article/edit');
    }

    /**
     * Módosítja egy publikáció adatait
     * @param $id
     */
    public function editSave($id)
    {
        if (isset($_POST['edit'])){

            //Fomról érkező adatok tömbe másolása //TODO tisztitás?
            $article['title'] = $_POST['title'];
            $article['abstract'] = $_POST['abstract'];
            $article['location'] = $_POST['location'];
            $article['source'] = $_POST['source'];
            $article['content'] = $_POST['content'];
            $article['year'] = $_POST['year'];

            $this->model->editSave($article, $id);
        }
    }

    /**
     * Meghívja a model törlés függvényét
     * @param $id a törlendő publikáció azonosítója
     */
    public function delete($id){

        Session::set('article','delete');
        $this->view->articleDetails = $this->model->articleDetailsShort($id);
        $this->view->renderAdmin('article/delete');
    }

    public function deleteConf($id){

        if(Session::get('article') == 'delete'){

            $this->model->delete($id);
            $this->model->deleteTag($id);
            $this->model->deleteAuthor($id);
            $this->model->deleteFile($id);

            Session::set('article',null);

            $this->view->msg = 'Sikeres törlés!';
            $this->view->renderAdmin('article/answer');
        }else{
            header('Location:'.URL.'article/manage');
        }
    }

    public function replace($string){

        $replacePairs= (array( 'á' => 'a' , 'Á' => 'A', 'é' => 'e',  'É' => 'E', 'Í' => 'I', 'í' => 'i',
            'ö' => 'o', 'Ö' => 'O', 'ü' => 'u', 'Ü' => 'U', 'ó' => 'o', 'Ó' => 'O', 'ő' => 'o', 'Ő' => 'O',
            'ű' => 'u','Ú' => 'U','ú' => 'u', 'Ű' => 'U',
        ));
        return strtr($string,$replacePairs);
    }

    ////-------Már nem használandó-------\\\\

    /**
     * Add By URl - URL alapú publikáció feltöltés
     * Feltölt az űrlapról érkező adatokkal egy tömböt, leellenőrzi, nincs-e hiányos adat
     * Meghívja az article_model addArticle függvényét az adatokkal - ez fogja az adatbázisba írást elvégezni
     */
    public function addByUrl()
    {

        if (!isset($_POST['add'])) {
            return false;
        } else {

            $errorOccurred = false; //Hibák jelzésére

            //Űrlapról érkező adatok kimentése TODO: adatok tisztitása? (admintól jön, csak nem XSS-zik)
            $article['title'] = $_POST['title'];
            $article['abstract'] = $_POST['abstract'];
            $article['location'] = $_POST['location'];
            $article['author'] = $_POST['author'];
            //TODO: $article['content'] = $_POST['content'];

            //Tage-ek tömbbe másolása
            $_POST['tags'] = rtrim($_POST['tags'], ",");     //Végéről vessző levágása, ha van
            $tags = explode(',', trim($_POST['tags']));      //vessző alapján felbontás

            //Elemek vizsgálata - egyik sem lehet üres. Ha valamelyik üres errorMsg feltöltése
            foreach ($article as $key => $value) {
                if (strlen($value) < 1) {
                    $errorOccurred = true;
                    $this->view->errorMsg[$key] = 'A ' . $key . ' mező nem lehet üres ';
                }
            }

            //Ha volt hiba, adatok visszaadása a formra és dataBack-el meghívni a view-t
            if ($errorOccurred) {
                //dataBack tömb feltöltése az article adatokkal - ez megy vissza a formra
                foreach ($article as $key => $value) {
                    $this->view->dataBack[$key] = $value;
                }
                $this->view->dataBack['tags'] = $_POST['tags'];
                $this->view->render('article/add');
            }
            else {
                //Ha nem volt hiba, adatok kiegészítése dátummal, fájlmutatóval
                $article['dateAdded'] = date('Y-m-d-h-m-s');
                $article['isFile'] = 0; //URL alapú hozzáadás, tehát nincs fájl hozzárendelve
                //TODO: $article['downloadable'] = 0;

                //Model meghívása, adatok adatbázisba írása
                $this->model->addArticleByUrl($article, $tags);
                header('Location:' . URL . 'articlelist');
            }
        }
    }

    /**
     * Fájl alapú publikáció feltöltés
     * Űrlapról érkező adatokkal feltölti az adatokat
     * Elvégzi a feltöltendő fájlokkal kapcsolatos ellenőrzéseket
     */

    public function addByFile()
    {

        if (isset($_POST['upload'])) {

            $errorOccurred = false; //Hibák jelzésére

            //Űrlapról érkező adatok kimentése TODO: adatok tisztitása? (admintól jön, csak nem XSS-zik)
            $article['title'] = $_POST['title'];
            $article['abstract'] = $_POST['abstract'];
            $article['author'] = $_POST['author'];

            //File adatai
            $files = $_FILES['file'];
            $temp = $files['tmp_name'];

            //Engedélyzett kiterjesztések, maximum méret TODO : esetleg configba
            $allowed_extensions = array('pdf', 'doc', 'docx');
            $sizeLimit = 50000000;

            //Kiterjesztés megvizsgálása
            $file_ext = $files['name'];
            $file_ext = explode('.', $file_ext);
            $file_ext = strtolower(end($file_ext)); //kisbetüssé a név utolsó pont utáni részét - ez lesz a kiterjesztés

            //Ha a kiterejesztés nem megengedett - hibaüzenet
            if (!in_array($file_ext, $allowed_extensions)) {
                $errorOccurred = true;
                $this->view->errorMsg['ext'] = 'Nem megengedett fájl kiterjesztés!';
            }
            //Ha a fájl túl nagy - hibaüzenet
            if ($files['size'] > $sizeLimit) {
                $errorOccurred = true;
                $this->view->errorMsg['size'] = 'Túl nagy fájl méret';
            }

            //Egyéni fájlnév generálása TODO : config fájlba az alapértelmezett útvonalat
            $file_name = uniqid('', true) . '.' . $file_ext;
            $destination = 'public/data/' . $file_name;

            //Ha nincs hiba, megkisereljuk a fajlfeltoltest
            if ($errorOccurred == false) {
                if (move_uploaded_file($temp, $destination)) {

                    //Article adatok kiegészítése
                    $article['location'] = $destination;   //Feltöltött fájl helye a szerveren
                    $article['dateAdded'] = date('Y-m-d-h-m-s');
                    $article['isFile'] = 1;

                    //Model meghívása, pubkiláció adatbázisba írása
                    $this->model->addArticleByUrl($article, $tags);
                    header('Location: ' . URL . 'articlelist');

                } else { //Hiba a fájlfeltöltés során
                    $errorOccurred = true;
                    $this->view->errorMsg['size'] = 'Valami hiba tortent a feltoltes soran';
                }
            }
            //Ha volt hiba, adatok visszaírása dataBack tömbbe - adatok vissza a form-ra
            if ($errorOccurred == true) {

                $this->view->dataBack['title'] = $article['title'];
                $this->view->dataBack['abstract'] = $article['abstract'];
                $this->view->dataBack['author'] = $article['author'];

                $this->view->render('article/upload');
            }
        }
    }
}