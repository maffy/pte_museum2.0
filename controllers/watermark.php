<?php

class Watermark extends Controller{
    /**
     * @var Watermark_Model
     */
    public $model;

    function __construct(){
        parent::__construct();
        Auth::handleLogin();
        Auth::role("watAcs");
    }

    /**
     * Vízjelek megjelenítése
     */
    function index(){
        //oldal címe
        $this->view->title = 'Admin - Vízjelek';

        //változóba a képek listáját
        $this->view->listElem = $this->model->selectByID("water");

        //megjelenítés
        $this->view->renderAdmin('admin/m_watermark');
    }

    /**
     * Hozzáad egy megadott képet
     */
    public function add(){
        Auth::role("watUpl");
        //szerverválaszok előkészítése
        $this->view->srvMsg = array();
        $this->view->srvNxt = "watermark/index";

        for( $i=0 ; $i<count($_FILES["file"]["name"]) && isset($_FILES["file"]["name"]) ; $i++ ){
            $waterIm["name"] = $_FILES["file"]["name"][$i];
            $waterIm["type"] = $_FILES["file"]["type"][$i];
            $waterIm["tmp_name"] = $_FILES["file"]["tmp_name"][$i];
            $waterIm["error"] = $_FILES["file"]["error"][$i];
            $waterIm["size"] = $_FILES["file"]["size"][$i];

            $this->view->srvMsg = Picture::save( $waterIm , WATER_PICTURES , 1 , Functions::getRCG() , true );
        }
        //válasz kiírása
        $this->view->renderAdmin("admin/m_answer");
    }

    /**
     * Törlés
     * @param $id
     */
    function delete( $id ){
        Auth::role("watDel");
        Session::set("act","preDelete");

        //id alapján kiszedjük az item sorát
        $this->view->exhElem = $this->model->selectByID( "water" , $id );
        $this->view->cancel = "watermark/index";
        $this->view->accept = "watermark/doDelete/";

        //megjelenítjük a megerősítőlapot
        $this->view->renderAdmin('admin/m_delete');
    }

    /**
     * Törlés biztonságosan
     * @param $id
     */
    function doDelete( $id ){
        //ezt azért hogy direktve ne törölhessünk
        if( Session::get("act")=="preDelete" ){
            //szerver válasz előkészítése
            $this->view->srvMsg = array();
            $this->view->srvMsg[0]["success"] = 1;
            $this->view->srvMsg[0]["message"] = "Sikeresen törölt elem!";
            $this->view->srvNxt = "watermark";

            //töröljük a file-t
            if( file_exists(WATER_PICTURES.$this->model->selectByID("water",$id)["image"]) ) {unlink(WATER_PICTURES.$this->model->selectByID("water",$id)["image"]);}

            //A főtáblában töröljük a sort
            $this->model->deleteByID( "water" , $id );

            $this->view->renderAdmin("admin/m_answer");
        } else {
            $this->view->renderAdmin("error/index");
        }
    }
}