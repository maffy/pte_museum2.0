﻿<?php
require __DIR__."/../config/config.php";
require __DIR__."/../libs/Hash.php";
require __DIR__."/../libs/Session.php";



Session::init();

if( isset($_GET["p"]) ) Session::set("p",$_GET["p"]);

if( Session::get("p")=="admin" ) {

    $ts["article"] = "CREATE TABLE `article` (
        `idarticle` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `title` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
        `abstract` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
        `content` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
        `source` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
        `location` text COLLATE utf8mb4_hungarian_ci NOT NULL,
        `filelocation` text COLLATE utf8mb4_hungarian_ci NOT NULL,
        `year` int(6) NOT NULL,
        `dateAdded` datetime NOT NULL,
        `isFile` int(2) NOT NULL DEFAULT '0'
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["role"] = "CREATE TABLE `role` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `uid` int(11)  NOT NULL,
        `exhAcs` tinyint(1) NOT NULL DEFAULT '1',
        `exhMod` tinyint(1) NOT NULL DEFAULT '1',
        `exhDel` tinyint(1) NOT NULL DEFAULT '1',
        `elmAcs` tinyint(1) NOT NULL DEFAULT '1',
        `elmUpl` tinyint(1) NOT NULL DEFAULT '1',
        `elmWat` tinyint(1) NOT NULL DEFAULT '1',
        `elmOwn` tinyint(1) NOT NULL DEFAULT '1',
        `watAcs` tinyint(1) NOT NULL DEFAULT '1',
        `watUpl` tinyint(1) NOT NULL DEFAULT '1',
        `watDel` tinyint(1) NOT NULL DEFAULT '1',
        `admAcs` tinyint(1) NOT NULL DEFAULT '1',
        `admMod` tinyint(1) NOT NULL DEFAULT '1',
        `admDel` tinyint(1) NOT NULL DEFAULT '1'
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["content"] = "CREATE TABLE `content` (
        `idcontent` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `page` varchar(30) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
        `title` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci,
        `content` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["article_tag"] = "CREATE TABLE `article_tag` (
        `articleid` int(11) NOT NULL,
        `tag` varchar(30) COLLATE utf8mb4_hungarian_ci NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["article_author"] = "CREATE TABLE `article_author` (
        `articleid` int(11) NOT NULL,
        `author` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["exhibit"] = "CREATE TABLE `exhibit` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `title` text COLLATE utf8_hungarian_ci NOT NULL,
        `auth` text COLLATE utf8_hungarian_ci NOT NULL,
        `disc` text COLLATE utf8_hungarian_ci NOT NULL,
        `mark` text COLLATE utf8_hungarian_ci NOT NULL,
        `date` date NOT NULL,
        `modified` date NOT NULL
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["mark"] = "CREATE TABLE `mark` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `code` text COLLATE utf8_hungarian_ci NOT NULL,
        `date` date NOT NULL
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["water"] = "CREATE TABLE `water` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `image` text COLLATE utf8_hungarian_ci NOT NULL,
        `date` date NOT NULL
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["user"] = "CREATE TABLE `user` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `username` text COLLATE utf8_hungarian_ci NOT NULL,
        `password` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
        `realname` text COLLATE utf8_hungarian_ci NOT NULL,
        `email` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
        `regip` varchar(16) COLLATE utf8_hungarian_ci NOT NULL,
        `regtime` date NOT NULL,
        `lastmodify` date NOT NULL,
        `lastin` date NOT NULL,
        `ban` int(2) NOT NULL DEFAULT '0'
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $ts["log"] = "CREATE TABLE `log` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `date` date NOT NULL,
        `time` time NOT NULL,
        `addr` varchar(16) COLLATE utf8_hungarian_ci NOT NULL,
        `data` text COLLATE utf8_hungarian_ci NOT NULL
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

    $apass = Hash::create("md5", "admin");
    $ts_value["user"] = "INSERT INTO `user` (`id`, `username`, `password`, `realname`, `email`, `regip`, `regtime`, `lastmodify`, `lastin`, `ban`) VALUES (1, 'admin', '$apass', 'Admin', '', '" . $_SERVER['REMOTE_ADDR'] . "', NOW(), NOW(), NOW(), 0);";
    $ts_value["role"] = "INSERT INTO `role` (`id`, `uid`, `exhAcs`, `exhMod`, `exhDel`, `elmAcs`, `elmUpl`, `elmWat`, `elmOwn`, `watAcs`, `watUpl`, `watDel`, `admAcs`, `admMod`, `admDel`) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);";
    $ts_value["content01"] = "INSERT INTO `content` (`idcontent`, `page`, `title`, `content`) VALUES (1, 'home', 'Informatika történeti múzeum', 'Nézzen körbe!')";
    $ts_value["content02"] = "INSERT INTO `content` (`idcontent`, `page`, `title`, `content`) VALUES (1, 'contact', 'Informatika történeti múzeum', 'Kapcsolatok!')";


///////////////////////////////
///////////////////////////////
///////////////////////Funkciók
///////////////////////////////
///////////////////////////////

    function drp($name){
        $sql = "DROP TABLE $name";
        updateSQL($sql);
    }

    function trn($name){
        $sql = "TRUNCATE TABLE $name";
        updateSQL($sql);
    }

    function makefile($name, $data){
        $myfile = fopen($name, "w") or die("Unable to open file!");
        fwrite($myfile, $data);
        fclose($myfile);
    }

    function updateDB(){
        $sql = "CREATE DATABASE IF NOT EXISTS ".DB_NAME;
        updateSQL($sql);
    }

    function updateSQL($sql, $ret = false , $all = true ){
        $db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        echo '<pre>';
        $sth = $db->prepare($sql);
        $sth->execute();
        if ($sth->errorInfo()[1] > 0) {
            print_r($sth);
            print_r($sth->errorInfo());
        } print_r("Sikeresen lefutott! ".$sql);
        if ($ret && $all) return $sth->fetchAll(PDO::FETCH_ASSOC);
        if ($ret && !$all) return $sth->fetch(PDO::FETCH_ASSOC);
    }

    function truncateAll(){
        $sql = "SHOW TABLES";
        $data = updateSQL($sql , true);
        for ($i = 0; $i < count($data); $i++) {
            $table = $data[$i]["Tables_in_" . DB_NAME];
            $sql = "TRUNCATE TABLE $table";
            updateSQL($sql);
        }
    }

    function dropAll(){
        $sql = "SHOW TABLES";
        $data = updateSQL($sql,true);
        for ($i = 0; $i < count($data); $i++) {
            $table = $data[$i]["Tables_in_" . DB_NAME];
            $sql = "DROP TABLE $table";
            updateSQL($sql);
        }
    }

   
    function updateAll($update = true, $ts, $ts_value){
        foreach ($ts as $key => $value) updateSQL($value);
        foreach ($ts_value as $key => $value) updateSQL($value);
    }

    ?>

    <!doctype html>
    <html>
    <head><title>Múzeum installer</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
    <body>
    <h3>Weboldal beállítása a szerverre</h3>
    <ol>
        <li>Győzödjünk meg róla hogy a <b><i>"config/config.php"</i></b> fájl létezik. <i>(ha nem hozzuk létre)</i></li>
        <li>Győzödjünk meg róla hogy a <b><i>".htaccess"</i></b> létezik és a tartalma a következő:
                    <pre>
            RewriteEngine on
            RewriteBase /

            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteCond %{REQUEST_FILENAME} !-l

            RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]
                    </pre>
        </li>
        <li>A <b><i>".htaccess"</i></b> fájlban a 2. sorhoz illesszük be annak a mappának az elérési utvonalát amelyben
            található. <i>(például: "/teszt/weboldal/" és a weboldal nevű mappában kell látnunk a ".htaccess" nevű
                fájlt)</i></li>
        <li>A <b><i>"config/config.php"</i></b> fájlban vihetjük fel a megfelelő paramétereket az adatbázishoz.</li>
        <li>A <b><i>"config/config.php"</i></b> továbbá beállíthatjuk az alapértelmezett útvonalakat.</li>
        <li>Figyelem: a config fájl tartalma három részre tagolható:
            <ul>
                <li>Módosítható: késöbbiekben funkcionális hibákat nem okoz!</li>
                <li>Csak óvatosan: lehetőség szerint csak a "telepítéskor módosítsuk", ellenkező esetben funkcionalitási
                    problémák léphetnek fel
                </li>
                <li>Nem módosítható: a rendszer működéséhez elengedhetetlen fájlok</li>
                <li>Megadható paraméterek a konfig urljéhez: ?url=&db_type=&db_host=&db_user=&db_pass=&db_name= </li>
             </ul>
        </li>
    </ol>
    <hr/>
    <h3>Táblák csoportos műveletek: </h3>

    <ul>
        <li><a href="installer.php?a=config">Config létrehozás default</a></li>
        <li><a href="installer.php?a=htaccess">.htacces létrehozás default</a></li>
        <?php
        if (file_exists(__DIR__ . "/../config/config.php")) {
            echo '<li><a href="installer.php?a=trnTables">Kiürítés</a></li>';
            echo '<li><a href="installer.php?a=drpTables">Eldobás</a></li>';
            echo '<li><a href="installer.php?a=updTables">Frissít/Kiegészít</a></li>';
            echo '<li><a href="installer.php?a=crtTables">Létrehoz</a></li>';
        }
        ?>
    </ul>

    <h3>Tábla műveletek: </h3>
    <?php
    $sql = "SHOW TABLES";
    $table = updateSQL($sql, true);

    echo '<table>';

    for ($i = 0; $i < count($table); $i++) {
        $name = $table[$i]['Tables_in_' . DB_NAME];
        $f1 = '<a href="installer.php?a=drp&t=' . $name . '"> Eldob </a>';
        $f2 = '<a href="installer.php?a=trn&t=' . $name . '"> Ürít </a>';
        echo '<tr>';
        echo '<td>';
        echo $name;
        echo '<td>';
        echo '<td>';
        echo $f1 . " | " . $f2;
        echo '<td>';
        echo '</tr>';
    }

    echo '</table>';
    ?>

    <h3>Sql lekérdezés: </h3>

    <form action="" method="POST">
        <input type="text" name="sql" style="width: 500px;"/>
        <input type="submit" name="query" value="Lekérdezés!"/>
    </form>

    <hr/>
    <h3>Táblákon végzett műveletek eredménye: </h3>


    <?php

///////////////////////////////
///////////////////////////////
/////////////////////GET & POST
///////////////////////////////
///////////////////////////////
    if (isset($_POST["query"])) {
        $sql = strip_tags(trim($_POST["sql"]));
        $f = updateSQL($sql, true );
        if( count($f) ) {
            echo '<table style="border: 1px solid black; text-align: center;">';
            for ($i = 0; $i < count($f); $i++) echo '<col width="100px">';
            echo '<thead>';
            echo '<tr>';
            foreach ($f[0] as $key => $val) {
                echo "<th>" . $key . "</th>";
            }
            echo '</tr>';
            echo '</thead>';

            for ($i = 0; $i < count($f); $i++) {
                echo '<col width="100px">';
                echo "<tr>";
                foreach ($f[$i] as $key => $val) {
                    echo "<td>" . $val . "</td>";
                }
                echo "</tr>";
            }
            echo '</table>';
        }
        print_r($f);
    }

    if (isset($_GET["a"])) {
        $a = $_GET["a"];
        if ($a == "crtTables"){
            updateAll(false, $ts, $ts_value);
        } elseif ($a == "updTables"){
            updateAll();
        } elseif ($a == "drpTables"){
            dropAll();
        } elseif ($a == "trnTables"){
            truncateAll();
        } elseif ($a == "config"){
            makefile(__DIR__ . "/config/config.php", $config);
        } elseif ($a == "htaccess"){
            makefile(__DIR__ . ".htaccess", $htaccess);
        } elseif ($a == "drp"){
            drp($_GET["t"]);
        } elseif ($a == "trn"){
            trn($_GET["t"]);
        }
    }

///////////////////////////////
///////////////////////////////
/////////////////////Deklarálás
///////////////////////////////
///////////////////////////////
    $htaccess = "
    RewriteEngine on
    RewriteBase /

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-l

    RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]";
    $config = "
<?php
//[ MÓDOSÍTHATÓ ]
	//FIGYELEM: ezt a .htaccess fájlban is állitsuk be általában csak a " / " kell
    //Alapértelmezett weboldalcím (/ jel a végén!) például: http://example.com/
    define('URL' , '" .(isset($_GET["url"]) ? ($_GET["url"]) : ("localhost")) . "/');

//[ CSAK ÓVATOSAN ]
    //Museum kép tárolásához utvonal - csak a legelső fájlfeltöltés elött, az adatbázisban nem teljes utvonalat határozzuk meg
    define('UPLOAD_PICT_DIR','public/image/exhibition/');

    //Ezek bármikor megváltoztathatók, de figyljünk oda, hogy a megadott utvonalon létezzen a fájl
    define('FILE_DEFAULT_WR','public/image/original/default.png');      //alapértelmezett WR: WRONG
    define('FILE_DEFAULT_NE','public/image/original/not-exist.png');    //nincs ilyen kép/nem létezik NE: NOT EXIST
    define('WATERMARK_PICT','public/image/original/watermark.png');     //vízjel
    define('WATER_PICTURES','public/image/watermarks/');       //vízjelek mappája

    //Időzóna beállítása
    date_default_timezone_set('UTC');

    //DATABASE
    define('DB_TYPE','" . (isset($_GET["db_type"]) ? $_GET["db_type"] : "mysql") . "');      //adatbátis típusa
    define('DB_HOST','" . (isset($_GET["db_host"]) ? $_GET["db_host"] : "localhost") . "');  //adatbázist futtató host/ipcím
    define('DB_USER','" . (isset($_GET["db_user"]) ? $_GET["db_user"] : "root") . "');       //adatbázis felhasználó
    define('DB_PASS','" . (isset($_GET["db_pass"]) ? $_GET["db_pass"] : "") . "');           //adatbázis felhasználójához tartozó jelszó
    define('DB_NAME','" . (isset($_GET["db_name"]) ? $_GET["db_name"] : "museum") . "');     //adatbázis neve

    //Speciális kódolás md5-ön felül
    define('HASH_KEY','fjriwew44334____43kfdd');

//[ NEM MÓDOSÍTHATÓ ]
    //Itt vannak a Core fájlok
    define('LIBS','libs/');";
}
?>
</body>
</html>

<?php
    if( Session::get("p")=="admin" )
    echo '
    <script type="text/javascript">
        ChangeUrl( "Múzeum installer" , "'.URL.'views/installer.php" );
        function ChangeUrl(title, url) {
            if (typeof (history.pushState) != "undefined") {
                var obj = { Title: title, Url: url };
                history.pushState(obj, obj.Title, obj.Url);
            } else {
                alert("A böngésző nem támogatja a HTML5-öt.");
            }
        }
    </script>';

?>