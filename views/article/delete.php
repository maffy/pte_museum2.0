<?php
if(isset($this->articleDetails)) {

    echo  '<div class="alert alert-warning text-center">';
    echo '<strong>Figyelem!</strong>Biztosan törli a kiválasztott elemet?';
    echo '</div>';

    ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Cím</th>
                <th>Abstract</th>
                <th>Tartalom</th>
                <th>Forrás</th>
                <th>Elérhetőség</th>
                <th>Megjelenés éve</th>
                <th>Törlés</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td><?=$this->articleDetails['idarticle']?></td>
            <td><?=$this->articleDetails['title']?></td>
            <td><?=$this->articleDetails['abstract']?></td>
            <td><?=$this->articleDetails['content']?></td>
            <td><?=$this->articleDetails['source']?></td>
            <td><?=$this->articleDetails['location']?></td>
            <td><?=$this->articleDetails['year']?></td>
            <td><a href="<?=URL?>article/deleteConf/<?=$this->articleDetails['idarticle']?>">Törlés</a></td>
        </tr>
        </tbody>
    </table>

    <?php
}
?>
