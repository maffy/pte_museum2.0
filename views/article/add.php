<meta charset="utf-8">
<h2>Publikáció hozzáadása</h2>

<?php

if(isset($this->errorMsg)) {

    echo 'Hiba a következő mezőknél :<br>';
    foreach ($this->errorMsg as $key => $value) {
        echo 'A ' . $key . ' mezo  : ' . $value . ' <br>';
    }
}
?>
<form method="post" action="<?php echo URL;?>article/addByUrl">
    <table name="articleAdd" border="1px solid">
        <tr>
            <td><label>Cím</label></td>
            <td><input type="text" name="title" value="<?php if(isset($this->dataBack['title'])) echo $this->dataBack['title'];?>"></td>
        </tr>
        <tr>
            <td><label>Elérhetőség (URL)</label></td>
            <td><input type="text" name="location" value="<?php if(isset($this->dataBack['location'])) echo $this->dataBack['location'];?>"></td>
        </tr>
        <tr>
            <td><label>Abstract</label><br></td>
            <td><textarea name="abstract"><?php if(isset($this->dataBack['abstract'])) echo $this->dataBack['abstract'];?></textarea></td>
        </tr>
        <tr>
            <td><label>Szerző(k)</label></td>
            <td><input type="text" name="author" value="<?php if(isset($this->dataBack['author'])) echo $this->dataBack['author'];?>"></td>
        </tr>
        <tr>
            <td><label>Tag-ek - vesszőval elválasztva</label></td>
            <td><input type="text" name="tags" value="<?php if(isset($this->dataBack['tags'])) echo $this->dataBack['tags'];?>"></td>
        </tr>
    </table>
    <input type="submit" name="add">
</form>
