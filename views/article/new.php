<div class="container text-center">

<h2>Publikáció feltöltése</h2>

<script src="//cdn.ckeditor.com/4.5.4/basic/ckeditor.js"></script>

<?php

if(isset($this->errorMsg)){

    foreach($this->errorMsg as $key => $value){
        echo '<div class="alert alert-danger">';
        echo '<strong>Hiba a '.$key.' mezőben</strong>'. $value;
        echo '</div>';
    }
}
?>

<div class="container">


<form class="form-horizontal" role="form" name="upload" action="<?=URL?>article/add" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="email">Cím</label>
            <input class="form-control" type="text" name="title" value="<?php if(isset($this->dataBack['title'])) echo $this->dataBack['title'];?>">
        </div>
        <div class="form-group">
            <label>Abstract</label>
            <textarea class="form-control" name="abstract"><?php if(isset($this->dataBack['abstract'])) echo $this->dataBack['abstract'];?></textarea>
        </div>
        <div class="form-group">
            <label>Tartalom</label>
            <textarea placeholder="Nem kötelező" name="content"><?php if(isset($this->dataBack['content'])) echo $this->dataBack['content'];?></textarea>
        </div>
        <div class="form-group"></div>
            <label for="email">Elérhetőség</label>
            <input class="form-control" type="text" name="location" placeholder="Ha nincs külső link kerjuk hagyja üresen">
        <div class="form-group">
            <label for="email">Forrás</label>
            <input class="form-control" type="text" name="source" placeholder="Ha nincs külső link kerjuk hagyja üresen">
        </div>
        <div class="form-group">
            <label for="email">Megjelenés éve</label>
            <input class="form-control" type="text" name="year" placeholder="Ha nincs külső link kerjuk hagyja üresen">
        </div>
        <div class="form-group">
            <label for="email">Szerzők - vesszővel elválasztva</label>
            <input class="form-control" type="text" name="authors" value="<?php if(isset($this->dataBack['authors'])) echo $this->dataBack['authors'];?>">
        </div>
        <div class="form-group">
            <label for="email">Tag-ek - vesszőval elválasztva</label>
            <input class="form-control" type="text" name="tags" value="<?php if(isset($this->dataBack['tags'])) echo $this->dataBack['tags'];?>">
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Fájl feltöltése</label>
            <input id="exampleInputFile" type="file" name="file" accept=".pdf, .doc, .docx, .txt">
        </div>
    <input class="btn btn-primary" type="submit" name="add">
</form>
</div>
</div>
<script>
    CKEDITOR.replace('abstract');
    CKEDITOR.replace('content');
    //CKEDITOR.replace('abstract');
</script>