<div class="container">
    <h2>Pubklikációk kezelése</h2>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Cím</th>
                <th>Abstract</th>
                <th>Forrás</th>
                <th>Elérhetőség</th>
                <th>Szerkesztés</th>
                <th>Törlés</th>
            </tr>
        </thead>
        <tbody>

    <?php
    //Táblázat tartalom
        if(isset($this->articleList)) {
            foreach ($this->articleList as $key => $article) {
                echo '<tr>';
                foreach ($article as $key =>  $value) {

                    echo '<td>'.$value.' </td>';

                }
                echo '<td><a href="' . URL . 'article/edit/' . $article['idarticle'] . '">Szerkesztés</a></td>';
                echo '<td><a href="' . URL . 'article/delete/' . $article['idarticle'] . '">Törlés</a></td>';
                echo '</tr>';
            }
        }
     ?>
        </tbody>
    </table>
</div>