<h2>Pubklikációk szerkeesztése</h2>
<?php

    if(!isset($this->articleDetails)){
        echo 'Nincs találat';
    }else {
        ?>
        <div class="container">
            <form class="form-horizontal container-fluid" role="form" method="post" action="<?php echo URL; ?>article/editSave/<?php echo $this->articleDetails['idarticle'] ?>">
                <div class="form-group">
                    <label for="email">Cím</label>
                    <input class="form-control" type="text" name="title"
                           value="<?php if (isset($this->articleDetails['title'])) echo $this->articleDetails['title']; ?>">
                </div>
                <div class="form-group">
                    <label>Abstract</label>
                    <textarea class="form-control" name="abstract"
                              value=""><?php if (isset($this->articleDetails['abstract'])) echo $this->articleDetails['abstract']; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Tartalom</label>
                    <textarea class="form-control" name="content"
                              value=""><?php if (isset($this->articleDetails['content'])) echo $this->articleDetails['content']; ?></textarea>
                </div>
                <div class="form-group">
                    <label for="email">Forrás</label>
                    <input class="form-control" type="text" name="source"
                           value="<?php if (isset($this->articleDetails['source'])) echo $this->articleDetails['source']; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Elérhetőség</label>
                    <input class="form-control" type="text" name="location"
                           value="<?php if (isset($this->articleDetails['location'])) echo $this->articleDetails['location']; ?>">
                </div>
                <div class="form-group">
                    <label>Megjelenés éve</label></td>
                    <input class="form-control" type="text" name="year"
                               value="<?php if (isset($this->articleDetails['year'])) echo $this->articleDetails['year']; ?>">
                </div>
                <input class="btn btn-primary" type="submit" name="edit">
            </form>
        </div>
        <?php
    }
?>