<meta charset="utf-8">
<h1>Cikkek listaja</h1>


<?php

echo '<a href="'.URL.'article/create">Add new article by URL</a><br>';
echo '<a href="'.URL.'article/upload">Upload a new article</a><br>';

echo '<hr>';

$articlesNum = (count($this->articleList)); //aRTICLErEKORDOK SZÁMA

?>

    <?php
        echo '<div id="articleDiv">';

        foreach($this->articleList as $key => $value){

            echo '<h2><a href="'. URL . 'article/details/' . $value['idarticle'] . '">' . $value['title'] . '</a></h2>';
            echo '<th>';
            echo '<p id="articleAbs">' . $value['abstract'] .'</p>';
            echo '<th>';

            if($value['isFile'] == 0){
                echo '<a href="http://'.$value['location'].'" target ="_blank">Megtekintes</a>';
            }else{
                echo '<a href="">Letöltés</a>';
            }
            echo '<div id="authorDiv"> Szerezte: <b>' . $value['author']. '</b></div>';

            echo '<hr>';
        }

        echo '</div>';

        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = 1;
        }

        $pages = getPages($page,10);
        foreach($pages as $key => $value){
            echo '<a style="font-size:22px" href ="'.URL.'article?page='.$value.'">'.$value.'</a>';
        }


    //FÜGGVÉNYEK:

    /*
     *
     *
     * TODO : lapozás lehetősége, ha sok article van
     * */
    /**
     * A jelenlegi oldalszámól kiszámolja, hogy milyen oldalra mutató linkeket kell generálni.@deprecated
     * Pl ha jelenlegi oldal a 3 -> Első 1 2 3 4 5 Utolsó
     * @param int $currPage jelenlegi oldalszám
     * @param int $maxPages Maximálisan szükséges oldalak (Cikkel számából lesz kiszámítva - pl egy odlalra 5 cikk kerüljön)
     * @return array() array - A megjelenítendő oldalszámokat, amiket legenerál
     */


    function getPages($currPage,$maxPages){

        $pages = array(); //A linkek számát tartlamazó tömb

        $linkNum = 5; //Ennyi oldal mutató link legyen összesen

        if($maxPages <= $linkNum){      //Szükséges oldalak száma <= mint ahány linket akarunk
            for($i = 1 ; $i <= $maxPages;$i++) {
                $pages[] = $i;
            }
        }else{
            $right = 0; //Linkek jobb oldali széle
            $left = 0; //Linkek bal oldali széle
            if($currPage > round($linkNum/2) ){      //Jobbra ugyaannyit kell "menni" mint balra
                $right = $currPage + (round($linkNum/2)-1);
                $left =  $currPage - (round($linkNum/2)-1);    //Összes link - amennyit balra mentünk - 1 ( currentPage miatt);
            }else{
                $left = 1;
                $right = $currPage + ($linkNum - $currPage);


            }
            if($right > $maxPages){     ///túlmentünk, ennyi már nem is kell
                $right = $maxPages;
            }

            for($i = $left; $i<= $right;$i++){
                $pages[] = $i;
            }
        }
        return $pages;
    }


/*
    function TODOPAGES(){

        if(isset($_GET['page'])){   //ha van page paraméter, és nem nagyobb, mint a MAX, akkor page paraméter az aktuális
            if($_GET['page'] > $pageMax){
                $currPage = $pageMax;
            }else{
                $currPage = $_GET['page'];
            }
        }else{
            $currPage = 1;
        }
        echo $currPage;
    }
*/
    ?>
