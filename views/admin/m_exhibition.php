<h2>Aktuális elemek</h2>
<div class="container">
    <div class="table-responsive">
        <table class="table table-striper table-hover">
            <col width="20px">
            <col width="100px">
            <col width="100px">
            <col width="*">
            <col width="100px">
            <col width="100px">
            <col width="200px">
            <?php
            if( count($this->listElem) ){
                echo '<thead>';
                echo '<tr>';
                echo '<th>ID</th>';
                echo '<th>Cím</th>';
                echo '<th>Szerző</th>';
                echo '<th>Leírás</th>';
                echo '<th>Hozzáadva</th>';
                echo '<th>Módosítva</th>';
                echo '<th>Műveletek</th>';
                echo '</tr>';
                echo '</thead>';
                for( $i=0 ; $i<count($this->listElem) ; $i++ ){
                    //rövidítések legenerálása
                    $discL = 80;
                    $short = $this->listElem[$i];
                    $short["title"] = ( strlen($this->listElem[$i]["title"])>$discL ) ?  substr($this->listElem[$i]["title"],0,$discL).'...' : $this->listElem[$i]["title"];
                    $short["auth"] = ( strlen($this->listElem[$i]["auth"])>$discL ) ? substr($this->listElem[$i]["auth"],0,$discL).'...' : $this->listElem[$i]["auth"];
                    $short["disc"] = ( strlen($this->listElem[$i]["disc"])>$discL ) ? substr(strip_tags($this->listElem[$i]["disc"]),0,$discL).'...' : strip_tags($this->listElem[$i]["disc"]);
                    $short["mark"] = ( strlen($this->listElem[$i]["mark"])>$discL ) ? substr($this->listElem[$i]["mark"],0,$discL).'...' : $this->listElem[$i]["mark"];

                    //műveletek meghatározása
                    $action1 = '<a href="'.URL.'museum/edit/'.$this->listElem[$i]["id"].'">Módosítás</a>';
                    $action2 = '<a href="'.URL.'museum/delete/'.$this->listElem[$i]["id"].'">Törlés</a>';
                    $action3 = '<a href="'.URL.'museum/check/'.$this->listElem[$i]["id"].'">Több</a>';

                    echo '<tr>';
                    echo '<td title="'.$this->listElem[$i]["id"].'">'.$this->listElem[$i]["id"].'</td>';
                    echo '<td title="'.$this->listElem[$i]["title"].'">'.$short["title"].'</td>';
                    echo '<td title="'.$this->listElem[$i]["auth"].'">'.$short["auth"].'</td>';
                    echo '<td title="'.strip_tags($this->listElem[$i]["disc"]).'">'.$short["disc"].'</td>';
                    echo '<td title="'.$this->listElem[$i]["date"].'">'.$short["date"].'</td>';
                    echo '<td title="'.$this->listElem[$i]["modified"].'">'.$short["modified"].'</td>';
                    echo '<td>'.$action1.' - '.$action2.' - '.$action3.'</td>';
                    echo '</tr>';
                }
            } else echo '<tr><td><center>Nincs megjeleníthető adat!</center></td></tr>';

            ?>
        </table>
    </div>
</div>