<p>Műveletek:
    <a href="../edit/<?=$this->exhElem["id"]?>">Módosít</a>
    <a href="../delete/<?=$this->exhElem["id"]?>">Töröl</a>
</p>



<p>ID:</p>
<?=$this->exhElem["id"]?>
<p>Szerző:</p>
<?=$this->exhElem["auth"]?>
<p>Cím:</p>
<?=$this->exhElem["title"]?>
<p>Leírás:</p>
<?=nl2br($this->exhElem["disc"])?>

<div class="container">
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th class="text-center">Eredeti kép</th>
                <th class="text-center">Vízjelezett kép</th>
            </tr>
            </thead>
            <?php
                for( $i=0 ; $i<count($this->exhIm) ; $i++ ){
                    echo '<tr>';
                    echo '<td class="text-center">';
                    echo '<img width="200px" src="'.URL.UPLOAD_PICT_DIR.$this->exhIm[$i]["file"].'"/>';
                    echo '</td>';
                    echo '<td class="text-center">';
                    echo '<img width="200px" src="'.URL.UPLOAD_PICT_DIR.$this->exhIm[$i]["mark"].'"/>';
                    echo '</td>';
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>