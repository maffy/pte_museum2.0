<h2>Aktuális elemek</h2>

<div class="container">
    <div class="table-responsive">
        <table class="table table-striper table-hover">
            <col width="20px">
            <col width="*">
            <col width="*">
            <col width="*">
            <col width="10%">
            <col width="10%">
            <col width="10%">
            <col width="10%">
            <col width="150px">
            <?php
            if( count($this->userList) ){
                echo '<thead>';
                echo '<tr>';
                echo '<th>ID</th>';
                echo '<th>Felhasználó</th>';
                echo '<th>Valódi név</th>';
                echo '<th>E-mail</th>';
                echo '<th>RegIP</th>';
                echo '<th>Regisztrált</th>';
                echo '<th>Módosítva</th>';
                echo '<th>Utoljára</th>';
                echo '<th>Műveletek</th>';
                echo '</tr>';
                echo '</thead>';

                for( $i=0 ; $i<count($this->userList) ; $i++ ){
                    //műveletek meghatározása
                    $action1 = '<a href="'.URL.'user/edit/'.$this->userList[$i]["id"].'">Módosítás</a>';
                    $action2 = '<a href="'.URL.'user/delete/'.$this->userList[$i]["id"].'">Törlés</a>';

                    echo '<tr>';
                    echo '<td>'.$this->userList[$i]["id"].'</td>';
                    echo '<td>'.$this->userList[$i]["username"].'</td>';
                    echo '<td>'.$this->userList[$i]["realname"].'</td>';
                    echo '<td>'.$this->userList[$i]["email"].'</td>';
                    echo '<td>'.$this->userList[$i]["regip"].'</td>';
                    echo '<td>'.$this->userList[$i]["regtime"].'</td>';
                    echo '<td>'.$this->userList[$i]["lastmodify"].'</td>';
                    echo '<td>'.$this->userList[$i]["lastin"].'</td>';
                    echo '<td>'.$action1.' - '.$action2.'</td>';
                    echo '</tr>';
                }
            } else echo '<tr><td><center>Nincs megjeleníthető adat!</center></td></tr>';

            ?>
        </table>
    </div>
</div>