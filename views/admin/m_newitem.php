<h2>Új elem hozzáadása</h2>

<script src="//cdn.ckeditor.com/4.5.4/basic/ckeditor.js"></script>

<form action="add" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="table-responsive">
            <table class="table">
                <col width="243px">
                <tr><td class="text-right"><label>Cím</label></td><td><input class="form-control" type="text" name="title" /></td></tr>
                <tr><td class="text-right"><label>Szerző</label></td><td><input class="form-control" type="text" name="auth" /></td></tr>
                <tr><td class="text-right"><label>Leírás</label></td><td><textarea type="text" cols="18" name="disc" ></textarea></td></tr>
                <tr><td class="text-right"><label>Hozzáadás dátuma</label></td><td><?=date('Y.m.d - (M D)')?></td></tr>
                <tr><td class="text-right"><label>Feltöltendő kép(ek)</label></td><td><input class="btn btn-primary" id="ims" multiple="multiple" accept=".jpg, .png, .gif, .jpeg" type="file" name="file[]" onchange="checkActives()" /></td></tr>
                <tr><td class="text-right"><label>Legyen(ek) a kép(ek) vízjelezve?</label></td><td><span>Igen</span><input id="waterWant0" type="radio" name="waterWant" value="1" onclick="checkActives()"/> <span>Nem</span><input id="waterWant1" type="radio" name="waterWant" value="0" onclick="checkActives()" checked /></td></tr>
                <tr><td class="text-right"><label>Vízjelezés módjának kiválasztása</label></td><td><span>Mozaik</span><input id="waterType0" type="radio" name="waterType" value="1" onclick="checkActives()"/> <span>Képméretű</span><input id="waterType1" type="radio" name="waterType" value="2" onclick="checkActives()"/> <span>Fix</span><input id="waterType2" type="radio" name="waterType" value="3" checked onclick="checkActives()" /></td></tr>
                <tr><td class="text-right"><label>Saját vízjel?</label></td><td><span>Igen</span><input id="waterOwner0" type="radio" name="waterOwner" value="1" onclick="checkActives()" /> <span>Nem</span><input id="waterOwner1" type="radio" name="waterOwner" value="0" onclick="checkActives()" checked /></td></tr>
                <tr><td class="text-right"><label>Saját vízjel kiválasztása</label></td><td><input class="btn btn-primary" id="waterImage" accept=".jpg, .png, .gif, .jpeg" type="file" name="waterImage" /></td></tr>
                <tr><td class="text-right"><label>Feltöltött vízjelekből választás</label></td>
                    <td>
                        <?php
                            if( count($this->listWater) ){
                                echo '<select class="btn btn-primary" id="waterSelected" name="waterSelected">';
                                echo '<option selected value="'.$this->listWater[0]['image'].'">'.$this->listWater[0]['image'].'</option>';
                                for( $i=1 ; $i<count($this->listWater) ; $i++ )
                                    echo '<option value="'.$this->listWater[$i]['image'].'">'.$this->listWater[$i]['image'].'</option>';

                                echo '</select>';
                            }
                        ?>
                    </td>
                </tr>
                <tr><td class="text-right"><label>Vízjel átlátszósága <span id="op">0</span></label></td><td><input id="range" type="range" max="100" min="0" step="10"  name="opacity" value="50" oninput="upd()"/></td></tr>
                <tr><td></td><td><input class="btn btn-primary" type="submit" name="submit" value="Hozzáad!" /></td></tr>
            </table>
        </div>
    </div>
</form>

<script>
    CKEDITOR.replace( "disc" );

    var ims = document.getElementById("ims");
	var op = document.getElementById("op");
	var range = document.getElementById("range");
    var waterWant0 = document.getElementById("waterWant0");
    var waterWant1 = document.getElementById("waterWant1");
    var waterType0 = document.getElementById("waterType0"); //mozaik
    var waterType1 = document.getElementById("waterType1"); //képméretű
    var waterType2 = document.getElementById("waterType2"); //fix
    var waterOwner0 = document.getElementById("waterOwner0");
    var waterOwner1 = document.getElementById("waterOwner1");
    var waterImage = document.getElementById("waterImage");
    var waterSelected = document.getElementById("waterSelected");
		
	//csuszka állapotának frissítése
	function upd(){
		op.innerHTML = range.value + "%";
	}
		
    function checkActives(){
        if( !ims.value ){
            waterWant0.disabled = true;
            waterWant1.disabled = true;
            waterWant1.checked = true;
        } else {
            waterWant0.disabled = false;
            waterWant1.disabled = false;
        }
        <?= count($this->listWater)==0 ? 'waterOwner0.checked = true; waterOwner1.disabled = true;' : "" ?>
        if( waterWant1.checked ){
			range.disabled = true;
            waterType0.disabled = true;
            waterType1.disabled = true;
            waterType2.disabled = true;
            waterOwner0.disabled = true;
            waterOwner1.disabled = true;
            <?= count($this->listWater)==0 ? 'waterOwner1.disabled = true;' : "" ?>
            waterImage.disabled = true;
            waterSelected.disabled = true;
        } else {
			range.disabled = false;
            waterType0.disabled = false;
            waterType1.disabled = false;
            waterType2.disabled = false;
            waterOwner0.disabled = false;
            waterOwner1.disabled = false;
			
            <?= count($this->listWater)==0 ? 'waterOwner1.disabled = false;' : "" ?>
            if( waterOwner1.checked ){
                waterImage.disabled = true;
                waterSelected.disabled = false;
            } else {
                waterImage.disabled = false;
                waterSelected.disabled = true;
            }
        }
        //document.getElementById("myBtn").disabled = true;
    }
	
	
	upd();
    checkActives();
</script>
