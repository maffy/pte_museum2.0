<?php
/**
 * használata
 *  $this->srvMsg[index]["success"] = 1; - ha az adott elem sikeres, amúgy 0
 *  $this->srvMsg[index]["message"] = ""; - szerver üzenete;
 *  $this->srvNextPage = ""; - következő oldal hivatkozása
 */

if( isset($this->srvNxt) ) {
    for ( $i=0 ; $i<count($this->srvMsg) ; $i++ ) {
        if ($this->srvMsg[$i]["success"]) {
            echo '<div class="alert alert-success">';
            echo '<strong>' . $this->srvMsg[$i]["message"] . '</strong>';
            echo '</div>';
        } else {
            echo '<div class="alert alert-warning">';
            echo '<strong>' . $this->srvMsg[$i]["message"] . '</strong>';
            echo '</div>';
        }
    }
    echo '<a href="' . URL . $this->srvNxt . '">Tovább...</a>';
} else {
    echo '<div class="alert alert-warning">';
    echo '<strong>A szerver nem küldött üzenetet!</strong>';
    echo '</div>';
}
?>