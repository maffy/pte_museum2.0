<h2>Aktuális elemek</h2>

<div class="container">
    <div class="table-responsive">
        <table class="table table-striper table-hover">
            <col width="20px">
            <col width="*">
            <col width="50px">
            <col width="50px">
            <?php
            if( count($this->listElem) ){
                echo '<thead>';
                echo '<tr>';
                echo '<th>ID</th>';
                echo '<th>Vízjel</th>';
                echo '<th>Kép</th>';
                echo '<th>Műveletek</th>';
                echo '</tr>';
                echo '</thead>';

                for( $i=0 ; $i<count($this->listElem) ; $i++ ){
                    //műveletek meghatározása
                    $action1 = '<a href="'.URL.'watermark/delete/'.$this->listElem[$i]["id"].'">Törlés</a>';

                    echo '<tr>';
                    echo '<td>'.$this->listElem[$i]["id"].'</td>';
                    echo '<td>'.$this->listElem[$i]["image"].'</td>';
                    echo '<td><img width="50px" height="50px" src="'.URL.WATER_PICTURES.$this->listElem[$i]["image"].'" /></td>';
                    echo '<td>'.$action1.'</td>';
                    echo '</tr>';
                }
            } else echo '<tr><td><center>Nincs megjeleníthető adat!</center></td></tr>';

            ?>
        </table>
    </div>
</div>
<form action="<?=URL?>watermark/add" method="post" enctype="multipart/form-data">
    <div class="container">
        <label>Új vízjel(ek) hozzáadása:</label><br/>
        <input class="btn btn-primary" multiple="multiple" accept=".jpg, .png, .gif, .jpeg" type="file" name="file[]" /><br/>
        <input class="btn btn-primary" type="submit" name="submit" value="Hozzáad!" /><br/>
    </div>
</form>