<form action="<?=URL?>user/modify/<?=$this->data["id"]?>" method="POST">
    <div class="container">
        <div class="table-responsive">
            <table class="table">
                <col width="150px">
                <tr>
                    <td>Valódi név</td>
                    <td><input class="form-control" type="text" name="realname" value="<?=$this->data["realname"]?>" placeholder="valódi név" size="20" /></td>
                </tr>
                <tr>
                    <td>E-mail cím</td>
                    <td><input class="form-control" type="text" name="email" value="<?=$this->data["email"]?>" placeholder="e-mail cím" /></td>
                </tr>
            </table>
        </div>
    </div>

    <h5>Jogosultságok</h5>

    <div class="table-responsive">
        <table class="table table-strip">
            <col width="300px">
            <tr>
                <td>Tárlat áttekintés menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["exhAcs"]==1?"checked":"")?> name="exhAcs" /></td>
            </tr>
            <tr>
                <td>Tárlat módosítás</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["exhMod"]==1?"checked":"")?> name="exhMod" /></td>
            </tr>
            <tr>
                <td>Tárlat törlés</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["exhDel"]==1?"checked":"")?> name="exhDel" /></td>
            </tr>
            <tr>
                <td>Új tárlatem menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["elmAcs"]==1?"checked":"")?> name="elmAcs" /></td>
            </tr>
            <tr>
                <td>Új tárlatelem feltöltés</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["elmUpl"]==1?"checked":"")?> name="elmUpl" /></td>
            </tr>
            <tr>
                <td>Vízjelezhet-e</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["elmWat"]==1?"checked":"")?> name="elmWat" /></td>
            </tr>
            <tr>
                <td>Saját vízjel</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["elmOwn"]==1?"checked":"")?> name="elmOwn" /></td>
            </tr>
            <tr>
                <td>Vízjel menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["watAcs"]==1?"checked":"")?> name="watAcs" /></td>
            </tr>
            <tr>
                <td>Vízjel feltöltés</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["watUpl"]==1?"checked":"")?> name="watUpl" /></td>
            </tr>
            <tr>
                <td>Vízjel törlése</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["watDel"]==1?"checked":"")?> name="watDel" /></td>
            </tr>
            <tr>
                <td>Admin menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["admAcs"]==1?"checked":"")?> name="admAcs" /></td>
            </tr>
            <tr>
                <td>Admin módosítása</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["admMod"]==1?"checked":"")?> name="admMod" /></td>
            </tr>
            <tr>
                <td>Admin törlése</td>
                <td><input class="checkbox" value="1" type="checkbox" <?=($this->data["admDel"]==1?"checked":"")?> name="admDel" /></td>
            </tr>
        </table>
        <input class="btn btn-primary" type="submit" value="Mentés!" />
</form>
</div>