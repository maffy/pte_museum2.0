    <form action="create" method="POST">
        <div class="container">
            <div class="table-responsive">
                <table class="table">
                    <col width="150px">
                    <tr>
                        <td>Valódi név</td>
                        <td><input class="form-control" type="text" name="realname" value="" placeholder="valódi név" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Felhasználónév</td>
                        <td><input class="form-control" type="text" name="username" value="" placeholder="felhasználónév" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Jelszó</td>
                        <td><input class="form-control" type="password" name="password" value="" placeholder="jelszó" size="20" /></td>
                    </tr>
                    <tr>
                        <td>Jelszó újra</td>
                        <td><input class="form-control" type="password" name="passconf" value="" placeholder="jelszó újra" size="20" /></td>
                    </tr>
                    <tr>
                        <td>E-mail cím</td>
                        <td><input class="form-control" type="text" name="email" value="" placeholder="e-mail cím" /></td>
                    </tr>
                </table>
            </div>
        </div>

        <h5>Jogosultságok</h5>

        <div class="table-responsive">
        <table class="table table-strip">
            <col width="300px">
            <tr>
                <td>Tárlat áttekintés menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="exhAcs" /></td>
            </tr>
            <tr>
                <td>Tárlat módosítás</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="exhMod" /></td>
            </tr>
            <tr>
                <td>Tárlat törlés</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="exhDel" /></td>
            </tr>
            <tr>
                <td>Új tárlatem menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="elmAcs" /></td>
            </tr>
            <tr>
                <td>Új tárlatelem feltöltés</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="elmUpl" /></td>
            </tr>
            <tr>
                <td>Vízjelezhet-e</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="elmWat" /></td>
            </tr>
            <tr>
                <td>Saját vízjel</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="elmOwn" /></td>
            </tr>
            <tr>
                <td>Vízjel menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="watAcs" /></td>
            </tr>
            <tr>
                <td>Vízjel feltöltés</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="watUpl" /></td>
            </tr>
            <tr>
                <td>Vízjel törlése</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="watDel" /></td>
            </tr>
            <tr>
                <td>Admin menüpont</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="admAcs" /></td>
            </tr>
            <tr>
                <td>Admin módosítása</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="admMod" /></td>
            </tr>
            <tr>
                <td>Admin törlése</td>
                <td><input class="checkbox" value="1" type="checkbox" checked name="admDel" /></td>
            </tr>
        </table>

        <input class="btn btn-primary" type="submit" value="Regisztráció" />

    </form>
</div>