<nav class="navbar navbar-default">
    <div class="container-fluid" >
        <div>
            <ul class="nav navbar-nav">
                <li><a href="<?=URL?>museum/exhibition">Tárlat Áttekintés</a></li>
                <li><a href="<?=URL?>museum/newitem">Új tárlatelem</a></li>
                <li><a href="<?=URL?>watermark">Vízjelek</a></li>
                <li><a href="<?=URL?>article/create">Új publikáció</a></li>
                <li><a href="<?=URL?>article/manage">Publikációk kezelése</a></li>
                <li><a href="<?=URL?>content/manage">Tartalom kezelés</a></li>
                <li><a href="<?=URL?>user/userlist">Admin kezelés</a></li>
                <li><a href="<?=URL?>user/createuser"><li>Új admin</a></li>
            </ul>
        </div>
    </div>
</nav>

