<!doctype html>
<html>
<head>
    <title>
        <?= isset($this->title) ? $this->title : "Múzeum"  ?>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="StyleSheet" type="text/CSS" href="<?=URL?>public/css/museum.css">
    <link rel="StyleSheet" type="text/CSS" href="<?=URL?>public/css/bootstrap.css">
    <link rel="StyleSheet" type="text/CSS" href="<?=URL?>public/css/css/bootstrap.min.css">

    <script src="<?=URL?>public/js/npm.js"></script>
    <script src="<?=URL?>public/js/jquery.js"></script>
    <script src="<?=URL?>public/js/bootstrap.js"></script>
    <script src="<?=URL?>public/js/bootstrap.min.js"></script>

    <link rel="icon" href="<?=URL?>public/image/original/favicon.ico" />
</head>
<body>
    <div class="container" >

        <div class="jumbotron" id="headerjumbotron">
            <div class="container text-center">
                <h1>Informatika történeti Múzeum</h1>
                <p>Galéria, Publikációk && sok más</p>
            </div>
        </div>

        <nav class="navbar navbar-default">
            <div class="container-fluid text-center" >
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Múzeum</a>
                </div>
                <div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?=URL?>">Kezdőlap</a></li>
                        <li><a href="<?=URL?>exhibit">Múzeum</a></li>
                        <li><a href="<?=URL?>articlelist">Cikkek</a></li>
                        <li><a href="<?=URL?>contact">Kapcsolat</a></li>
                        <?php if( Session::get("loggedIn")==true ){
                            echo '<li><a href="'.URL.'admin" >Admin</a></li>';
                            echo '<li><a href="'.URL.'login/logout" >Kijelentkezés</a></li>';
                        }?>
                    </ul>
                </div>
            </div>
        </nav>