<div class="container text-center">
<?php
$articlesNum = (count($this->articleList)); //Articlerekordok száma
$currpage = isset($_GET['page']) ? $_GET['page'] : 1; //Jelenlegi oldalszám
$currpage = $currpage -1;
$publications = 4; //Ennyi cikket listázunk egy oldalra

$first = $currpage * $publications;
$last = $first+ $publications;
if($articlesNum < $last){
    $last = $articlesNum;
}
$maxpage = ceil($articlesNum/$publications);
$pages = Functions::getPages($currpage,$maxpage);

?>

<?php
for($i = $first ;$i<$last;$i++){

    echo '<div class="well">';

    echo '<h2><a href="'. URL . 'articlelist/details/' . $this->articleList[$i]['idarticle'] . '">' . $this->articleList[$i]['title'] . '</a></h2>';
    echo '<th>';
    echo '<p id="articleAbs">' .$this->articleList[$i]['abstract'] .'</p>';
    echo '<th>';

    if($this->articleList[$i]['isFile'] == 0){
        echo '<a href="'.$this->articleList[$i]['location'].'" target ="_blank">Megtekintes</a>';
    }else{
        echo '<a href="'.URL . $this->articleList[$i]['filelocation'].'">Letöltés</a>';
    }
    echo '</div>';
}
if(count($pages) > 1) {
    echo '<ul class="pagination">';

    foreach ($pages as $value) {

        echo '<li><a href ="' . URL . 'articlelist?page=' . $value . '">' . $value . '</a></li>';
    }

    echo '</ul>';
}
?>
    <div class="container">
            <form class="form-inline" name="search" action="<?=URL?>articlelist/search" method="post">
                <div class="form-group">
                    <label for="email">Keresés</label>
                    <input class="form-control" type="text" name="text">
                </div>
                <div class="form-group">
                    <select class="form-control" name="searchby">
                    <option value="author">Szerző</option>
                    <option value="tag">Témakör</option>
                    </select>
                </div>
                <input class="btn btn-primary" type="submit" name="search" value="Keresés">
    </div>
</div>
<?php

//----------FÜGGVÉNYEK--------//

/*
 * TODO : lapozás lehetősége, ha sok article van
 *
 * A jelenlegi oldalszámból kiszámolja, hogy milyen oldalra mutató linkeket kell generálni.
 * Pl ha jelenlegi oldal a 3 -> Első 1 2 3 4 5 Utolsó
 * @param int $currPage jelenlegi oldalszám
 * @param int $maxPages Maximálisan szükséges oldalak (Cikkek számából lesz kiszámítva - pl egy odlalra 5 cikk kerüljön)
 * @return array() array - A megjelenítendő oldalszámok tömbje, amiket legenerál
 */


function getPages($currPage,$maxPages){

    $pages = array(); //A linkek számát tartlamazó tömb

    $linkNum = 5; //Ennyi oldal mutató link legyen összesen

    if($maxPages <= $linkNum){      //Szükséges oldalak száma <= mint ahány linket akarunk
        for($i = 1 ; $i <= $maxPages;$i++) {
            $pages[] = $i;
        }
    }else{
        $right = 0; //Linkek jobb oldali széle
        $left = 0; //Linkek bal oldali széle
        if($currPage > round($linkNum/2) ){      //Jobbra ugyaannyit kell "menni" mint balra
            $right = $currPage + (round($linkNum/2)-1);
            $left =  $currPage - (round($linkNum/2)-1);    //Összes link - amennyit balra mentünk - 1 ( currentPage miatt);
        }else{
            $left = 1;
            $right = $currPage + ($linkNum - $currPage);


        }
        if($right > $maxPages){     ///túlmentünk, ennyi már nem is kell
            $right = $maxPages;
        }

        for($i = $left; $i<= $right;$i++){
            $pages[] = $i;
        }
    }
    return $pages;
}
