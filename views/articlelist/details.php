<div class="container text-center">


<?php
    //Változók létrehozása indexek alapján, hogy olvashatóbb legyen a kód
    if(!empty($this->articleDetails)) {
        foreach ($this->articleDetails as $key => $value) {
            $$key = $value;
        }
?>

    <h2><?= $title ?></h2>

    <h3>Abstract</h3>

    <div class="well-sm">
        <?= $abstract ?>
    </div>

    <h3>Tartalom</h3>

    <p class="content_Details">
        <?= $content ?>
    </p>

        <?php
        foreach ($this->articleAuthors as $authors) {
            foreach ($authors as $author) {
                $author_wrt = str_replace('_',' ',$author);
                echo '<a href="' . URL . 'articlelist/searchByAuthor/' . $author . '">' . $author_wrt . '</a>';
                echo ' ';
            }
        }
    ?>

    <p class="date_details">Megjelenés éve: <?= $year ?></p>

    <?php

        if(count($this->articleTags)){
            echo 'Témakörök:';

            foreach ($this->articleTags as $tags) {
                foreach ($tags as $tag) {
                    echo '<a href="' . URL . 'articlelist/searchByTag/' . $tag . '">' . $tag . '</a>';
                    echo ' , ';
                }
            }
        }


    if ($isFile == 1) {
        echo '<a href="'.URL.$filelocation.'">Letöltés</a>';
    }
    if($location != ''){

        echo '<a href="' . $location . '" target="_blank">Megtekintés a weblapon</a>';
    }

    echo '<br>';

    echo 'Forrás: <br>';
    echo $source;
}else{
    echo '<h2>Nincs találat!</h2>';
}

?>
</div>

