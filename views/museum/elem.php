<div class="container">
    <?php
        $detail = $this->exhElem;
        $style = "";
        $haveImg = false;
        $haveAuth = false;
        $haveTitl = false;
        $haveDisc = false;
        $auth = trim(strip_tags(Functions::filter($detail["auth"])));
        $disc = trim(strip_tags($detail["disc"]));
        $titl = trim(strip_tags(Functions::filter($detail["title"])));

        if( strlen($auth) ) $haveAuth = true;
        if( strlen($titl) ) $haveTitl = true;
        if( strlen($disc) ) $haveDisc = true;

        if( count($this->pic) ){
            $haveImg = true;
        }

        if( $haveImg ) {
            echo '<div id="sl" class="carousel slide" data-ride="carousel">';

            if( $this->slide ) {
                echo '<ol class="carousel-indicators">';
                echo '<li data-target="#sl" data-slide-to="0" class="active"></li>';
                for ($i = 1; $i < count($this->pic); $i++) {
                    echo '<li data-target="#sl" data-slide-to="' . $i . '"></li>';
                }
                echo '</ol>';
            }
            echo '<div class="carousel-inner" role="listbox">';
            echo '<div class="item active im1">';
            echo '<img src="' . $this->pic[0] . '">';
            echo '</div>';
            for ($i = 1; $i < count($this->pic); $i++) {
                echo '<div class="item">';
                echo '<img src="' . $this->pic[$i] . '">';
                echo '</div>';
            }
            echo '</div>';

            if( $this->slide ){
                echo '<a class="left carousel-control" href="#sl" role="button" data-slide="prev">';
                echo '    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>';
                echo '    <span class="sr-only">Previous</span>';
                echo '</a>';
                echo '<a class="right carousel-control" href="#sl" role="button" data-slide="next">';
                echo '    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>';
                echo '    <span class="sr-only">Next</span>';
                echo '</a>';
            }
            echo '</div>';
        }

        if( $haveAuth ) echo '<h3>'.$this->exhElem["auth"].'</h3>';
        if( $haveTitl ) echo '<h3>'.$this->exhElem["title"].'</h3>';
        if( $haveDisc ) echo nl2br($this->exhElem["disc"]);

        if( !$haveAuth && !$haveTitl && !$haveDisc ){
            $style .= '.carousel{';
            $style .= 'width: 500px';
            $style .= '}';
        } else {
            $style .= '.carousel{';
            $style .= 'width: 300px;';
            $style .= 'float: right;';
            $style .= 'position: relative;';
            $style .= '}';
        }
    ?>
</div>
<style>
    <?=$style?>
</style>