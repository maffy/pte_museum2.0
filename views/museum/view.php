
<div class="container">
    <?php
    $discL = 200;
    $style="";

    for( $i=0 ; $i<count($this->exhElem) ; $i++ ) {
        $text = $this->exhElem[$i];

        $detail = $this->exhElem[$i];
        $haveTxt = false;
        $haveImg = false;

        $auth = trim(strip_tags(Functions::filter($detail["auth"])));
        $disc = trim(strip_tags($detail["disc"]));
        $titl = trim(strip_tags(Functions::filter($detail["title"])));

        if( strlen($titl) ) $haveTxt = true;
        if( strlen($auth) ) $haveTxt = true;
        if( strlen($disc) ) $haveTxt = true;
        if( isset($this->exhElem[$i]["im"][0]) ){
            $image = $this->exhElem[$i]["im"][0];
            $haveImg = true;
        }

        if( $haveImg || $haveTxt ) {
            echo '<a href="exhibit/view/' . $this->exhElem[$i]["id"] . '">';
            echo '<div class="bg-primary mbox">';
        }

        if ( $haveImg ){
            echo '<div class="'.(!$haveTxt?"mbox-top-notext":"").' mbox-top'.$i.' mbox-top">';
            $appear = ($image["file"]!="") ? (file_exists(UPLOAD_PICT_DIR.$image["mark"]) ? URL.UPLOAD_PICT_DIR.$image["mark"] : URL.FILE_DEFAULT_WR) : URL.FILE_DEFAULT_NE;
            if( $haveTxt ) $style .= '.mbox-top' . $i . '{ background-image: url("' . $appear . '"); }';
            else $style .= '.mbox-top' . $i . '{ background-image: url("' . $appear . '"); }';
            echo '</div>';
        }

        if( $haveTxt ){
            echo '<div class="mbox-bot">';
            echo '<div style="padding: 10px;">';
            echo '<center>';
            echo '<div>';
            echo ($auth!="" ? ("Szerző: ".substr($auth,0,20)) : (""));
            echo '<br/>';
            echo ($titl!="" ? ("Cím: ".substr($titl,0,20)) : (""));
            echo '</div>';
            echo '<hr noshade />';
            echo '</center>';
            echo '<div style="font-size: 10px;">' . ($disc!="") ? (strlen($disc)>$discL) ? (substr($disc,0,(!$haveImg?1000:$discL))."...") : ($disc) : ("").'</div>';
            echo '</div>';
            echo '</div>';
        }

        if( $haveImg || $haveTxt ){
            echo '</div>';
            echo '</a>';
        }
    }
    ?>
</div>

    <style>
        <?=$style?>
    </style>

    <!-- FOOTER RÉSZ -->
    <center>