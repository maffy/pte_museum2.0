    <div class="container">
        <h2>Kezdőlap tartalmának módosítása</h2>
        <form class="form-horizontal" role="form" method="POST" action="<?=URL?>content/update/home">
            <div class="form-group">
                <label for="email">Kezdőlap címe</label>
                <input class="form-control" type="text" name="title" value="<?php if (isset($this->contentHome['title'])) echo $this->contentHome['title'];?>">
            </div>
            <div class="form-group">
                <label for="email">Kezdőlap tartalma</label>
                <textarea class="form-control" type="text" name="content" ><?php if (isset($this->contentHome['content'])) echo $this->contentHome['content'];?></textarea>
            </div>
            <input class="btn btn-primary" type="submit" name="update" value="Mentés">
        </form>
    </div>
    <div class="container">
        <h2>Kapcsolat oldal tartalmának módosítása</h2>
        <form class="form-horizontal" role="form" method="POST" action="<?=URL?>content/update/contact">
            <div class="form-group">
                <label for="email">Kapcsolat oldal címe</label>
                <input class="form-control" type="text" name="title" value="<?php if (isset($this->contentContact['title'])) echo $this->contentContact['title'];?>">
            </div>
            <div class="form-group">
                <label for="email">Kapcsolat oldal tartalma</label>
                <textarea class="form-control" type="text" name="content" ><?php if (isset($this->contentContact['content'])) echo $this->contentContact['content'];?></textarea>
            </div>
            <input class="btn btn-primary" type="submit" name="update" value="Mentés">
        </form>
    </div>
