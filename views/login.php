<div class="container text-center" id="logindiv">
    <h2>Bejelentkezés</h2>
    <form class="form-horizontal" action="login/run" method="POST">
           <div class="form-group-lg">
                <input type="text" class="form-control" name="username" placeholder="Felhasználónév">
           </div>
            <div class="form-group-lg">
                <input type="password" class="form-control" name="password" placeholder="Jelszó">
            </div>
        <input type="submit" class="btn btn-primary btn-lg" name="submit">
    </form>
</div>