<?php

class Hash {
    /**
     * @param $algo - Milyen algoritmust használunk -> pl: md5
     * @param $data - Az adat amelyet kódolunk
     */

    public static function create($algo, $data){
        $context =  hash_init($algo,HASH_HMAC,HASH_KEY);
        hash_update($context,$data);
        return hash_final($context);
    }
}