<?php

class View {
    function __construct(){
       // echo 'Ez a View';
    }

    /**
     * Egyszerű megjelenítő fv
     * @param $name
     * @param bool|false $noInclude
     */
    public function render($name, $noInclude = false){
        if($noInclude){
            require 'views/' .$name . '.php';
        }else{
            require 'views/data/header.php';
            require 'views/data/content.php';
            require 'views/' .$name . '.php';
            require 'views/data/footer.php';
        }
    }
    
    /**
     * Specifikusan az admin oldalon való navigáláshoz
     * @param string $name
     * @param boolean $noInclude
     */
    public function renderAdmin( $name=null, $noInclude = false ){
        Auth::handleLogin();
        if( $noInclude ){
            if( $name!=null ) require 'views/'.$name.'.php';
        } else {
            require 'views/data/header.php';
            require 'views/admin/menu.php';
            require 'views/data/content.php';
            if( $name!=null ) require 'views/' .$name . '.php';
            require 'views/data/footer.php';
        }
    }
}