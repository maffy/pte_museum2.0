<?php

class Controller{
    //ezt annotáljuk a könnyebb programozáshoz
    public $model;

    function __construct(){
        //hozzuk létre a view szintet
        $this->view = new View();
    }

    /**
     * Töltsük be a model osztályt a kontrollerhez ha van olyan
     * @param $name
     */
    public function loadModel($name){
        $path = 'models/'.$name.'_model.php';
        if(file_exists($path)){
            require 'models/'.$name.'_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }
}