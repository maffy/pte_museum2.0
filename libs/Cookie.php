<?php

class Cookie{

    /** Megadhatunk �j cookie v�ltoz�kat
     * @param $key
     * @param $value
     * @param $minute
     */
    public static function set( $key , $value , $minute=5 ){
        //setcookie(name, value, expire, path, domain, secure, httponly);
        setcookie( $key , $value , time() + ($minute * 60) );
    }

    /** Lek�rhetj�k a l�tez� cookie v�ltoz�kat
     * @param $key
     * @return mixed
     */
    public static function get( $key ){
        if( isset($_COOKIE[$key]) ){
            return $_COOKIE[$key];
        } else return false;
    }

    /**
     * A l�tez� cookie t�rl�se
     * @param $key
     */
    public static function destroy( $key ){
        setcookie( $key , null , -1 );
        unset($_COOKIE[$key]);
    }
}