<?php
require ('Hash.php');

class Database extends PDO{
    public function __construct($dbType,$dbHost,$dbName,$dbUser,$dbPass){
        try{
            //a config-ban megadott adatok alapján csatlakozzunk az adatbázishoz
            parent::__construct($dbType.':host='.$dbHost.';dbname='.$dbName,$dbUser,$dbPass );
        } catch( PDOException $e ) {
            echo '<pre>';
            print_r($e);
            echo '</pre>';
        }
    }
    
    /**
     * insertDyn - Dinamikusan beszúró metódus, felállítja az SQL parancsot a megadott adatokkal
     * @param string $table - Táblanév
     * @param  array $data - Adatok tömbje /  keys = tableFields, values = tableValues
     */
    public function insertDyn( $table,$data ){
        $fieldNames = implode(',',array_keys($data));
        $fieldValues = ':' .  implode(', :',array_keys($data));
        $statement = $this->prepare('INSERT INTO ' . $table . '(' . $fieldNames . ') VALUES(' . $fieldValues . ')');
        foreach ($data as $key => $value ) {
            $statement->bindValue(':' . $key,$value);
        }
        $statement->execute();
    }

    /**
     * updateDyn - dinamikusan frissíti a tábla tartalmát a megadott adatokkal
     * @param string $table - Táblanév
     * @param array $data - Adatok tömbje
     * @param string $where - Hol tegyük meg a frissítést
     */
    public function updateDyn($table,$data,$where){
        $fieldDetails = '';
        foreach ($data as $key => $value) {
            $fieldDetails = $fieldDetails . $key . ' = :' . $key . ', ';
        }
        $fieldDetails = rtrim($fieldDetails, ', ');
        $statement = $this->prepare('UPDATE ' . $table . ' SET ' . $fieldDetails . ' WHERE ' . $where);

        foreach ($data as $key => $value) {
            $statement->bindValue(':' . $key,$value);
        }

        $statement->execute();
    }

    /**
     * Egyszerű lekérdezésekhez
     * @param  string $sql - Az SQL parancs
     * @param  array $array - Paraméterek listája
     * @param  constant $fetchMode - FETCH mód kiválasztása, a default érték -> PDO::FETCH_ASSOC
     * @return array - Tömb amelyben az adatok vannak
     */
    public function select($sql,$array = array() ,$fetchMode = PDO::FETCH_ASSOC){
        $statement = $this->prepare($sql);
        foreach($array as $key => $value){
            $statement->bindValue($key , $value);
        }
        $statement->execute();
        return $statement->fetchAll($fetchMode);
    }

    /**
     * Egyszerű törlés
     * @param $sql
     * @return array
     */
    public function delete( $sql ){
        $statement = $this->prepare( $sql );
        $statement->execute();
        return $statement->errorInfo();
    }

    /**
     * Egyszerű beszúrás
     * @param $table
     * @param $data
     */
    public function insert($table,$data){
        ksort($data);
        $statement = $this->prepare("INSERT INTO $table (title,content,date_added) VALUES (:title, :content, :date_added)");
        $statement->execute(array(
            ':title' => $data['title'],
            ':content' => $data['content'],
            ':date_added' => $data['date_added']
        ));
    }

    /**
     * Hozzáad az adatbázishoz egy article rekordot, $data-ban lévő adatokkal
     * Hozzáadja a $tags-ben tárolt tag-eket az article_tag táblához, idegen kulcssal
     * @param $data - az article adatai
     * @param $tags - az article tag-ek
     *
     */
    public function insertArticleWithTags($data,$tags){

        //Article rekord hozzáadása
        $fieldNames = implode(',',array_keys($data));
        $fieldValues = ':' .  implode(', :',array_keys($data));
        $statement = $this->prepare('INSERT INTO article (' . $fieldNames . ') VALUES(' . $fieldValues . ')');
        foreach ($data as $key => $value ) {
            $statement->bindValue(':' . $key,$value);
        }
        $statement->execute();

        //Utoljára hozáadott rekord id-je - ez lesz az idegen kulcs a article_tag táblában
        $lastId = $this->lastInsertId();

        foreach ($tags as $value ) {

            $statement = $this->prepare('INSERT INTO article_tag (articleid,tag) VALUES (:articleid, :tag)');
            $statement->execute(array(
                ':articleid' => $lastId,
                ':tag' => $value
            ));
        }
    }

    public function selectSingle($sql,$array = array(),$fetchmode = PDO::FETCH_ASSOC){

        $statement = $this->prepare($sql);
        if(!empty($array)){
            foreach($array as $key => $value){
                $statement->bindValue(':'.$key,$value);
            }
        }
        $statement->execute();

        return $statement->fetch($fetchmode);

    }

    public function addArticle($data,$tags,$authors){

        $fieldNames = implode(',',array_keys($data));
        $fieldValues = ':' .  implode(', :',array_keys($data));
        $statement = $this->prepare('INSERT INTO article (' . $fieldNames . ') VALUES(' . $fieldValues . ')');
        foreach ($data as $key => $value ) {
            $statement->bindValue(':' . $key,$value);
        }
        $statement->execute();

        //Utoljára hozáadott rekord id-je - ez lesz az idegen kulcs a article_tag táblában
        $lastId = $this->lastInsertId();

        if(!empty($tags)){

            $statementtag = $this->prepare('INSERT INTO article_tag (articleid,tag) VALUES (:articleid, :tag)');
            foreach ($tags as $value ) {

                $statementtag->execute(array(
                    ':articleid' => $lastId,
                    ':tag' => $value
                ));
            }
        }

        if(!empty($authors)){

            foreach($authors as $key => $value){

                $authors[$key] = str_replace(' ','_',$value);
            }

            $statementauth = $this->prepare('INSERT INTO article_author (articleid,author) VALUES (:articleid, :author)');
            foreach ($authors as $value ) {

                $statementauth->execute(array(
                    ':articleid' => $lastId,
                    ':author' => $value
                ));
            }
        }
    }
}