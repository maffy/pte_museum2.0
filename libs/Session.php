<?php

class Session{

    /**
     * Létrehozzuk a session-t a felhasználónak
     */
    public static function init(){
		@session_start();
    }

    /** Megadhatunk új session változókat
     * @param $key
     * @param $value
     */
    public static function set( $key , $value ){
        $_SESSION[$key] = $value;
    }

    /** Lekérhetjük a létező session változókat
     * @param $key
     * @return mixed
     */
    public static function get( $key ){
        if( isset($_SESSION[$key]) ){
            return $_SESSION[$key];
        } else return false;
    }

    /**
     * Logout, vagy feleslegesen indított session-ok törlése
     */
    public static function destroy(){
        session_destroy();
    }
}