<?php

class Bootstrap {

    private $_url = null; //védett url
    private $_controller = null;
    private $_hasError = false; //true ha van hiba, error

    function __construct(){
        //állítsuk be a védett url-ünk;
        $this->_getUrl();

        //hívjuk meg az alapértelmezett kontrollert ha az url "üres" -> index
        if( empty($this->_url[0]) ){
            $this->_loadDefaultController();
            return false;
        }

        //ha adtunk meg osztályt akkor azt a kontrollert tölti be
        $this->_loadCurrentController();

        //hívjuk meg a hozzátartozó metódust
        if( !$this->_hasError ){
            Log::setLog(implode("/",$this->_url));
            $this->_callControllerMethod();
        }
    }

    /**
     * Mentsük az URL-ünk egy privát változóba
     */
    private function _getUrl(){
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url,FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }

    /**
     * Ez az alapértelmezett kontroller, ez az index oldal
     */
    private function _loadDefaultController(){
        require 'controllers/index.php';
        $controller = new Index();
        $controller->loadModel('index');
        $controller->index();
    }

    /**
     * Ha volt akkor hívjuk meg a kontrollerünk és állítsuk fel az összeköttetést a szintek között
     * @return bool - visszatér ha van hiba
     */
    private function _loadCurrentController(){
        $file = 'controllers/' . $this->_url[0] . '.php';
        if (file_exists($file)) {
            require $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->loadModel($this->_url[0]);
        } else {
            $this->error();
            return false;
        }
    }

    /**
     * url[0] CONTROLLER
     * url[1] METHOD
     * url[2] PARAM1
     */

    /**
     * Meghívjuk a metódust ha volt
     * @return bool
     */
    private function _callControllerMethod(){
        $length = count($this->_url); //számoljuk meg hány érték van az url-ben

        //ha több mint 1
        if($length > 1 ){
            if( !method_exists($this->_controller,$this->_url[1]) ){
                $this->error();
                return false;
            }
        }

        //a megfelelő hosszúsághoz
        switch($length){
            case 5:
                // Controller->Method(Param1,Param2,Param3)
                $this->_controller->{$this->_url[1]}($this->_url[2],$this->_url[3],$this->_url[4]);
                break;
            case 4:
                // Controller->Method(Param1,Param2)
                $this->_controller->{$this->_url[1]}($this->_url[2],$this->_url[3]);
                break;
            case 3:
                // Controller->Method(Param1)
                if($this->validUrl($this->_controller,$this->_url[1],($length-2))){
                    $this->_controller->{$this->_url[1]}($this->_url[2]);
                } else {
                    $this->error();
                }
                break;
            case 2:
                // Controller->Method()
                if($this->validUrl($this->_controller,$this->_url[1],($length-2))){
                    $this->_controller->{$this->_url[1]}();
                } else {
                    $this->error();
                }
                break;
            default:
                $this->_controller->index();
        }
    }

    /**
     * Hiba
     * @return bool
     */
    function error(){
        require 'controllers/error.php';
        $this->_controller = new Error();
        $this->_controller->index();
        $this->_hasError = true;
        return false;
    }

    /**
     * Megnézi hogy biztos jól hívták e meg a az oszályt és a metódust
     * @param string $class - Ez az osztály név
     * @param string $method - Ez a metódus név
     * @param $paramNum - A paraméterek száma a metódusban
     * @return boolean
     */
    function validUrl( $class , $method , $paramNum ){
        $controllerMethod = new ReflectionMethod( $class , $method );
        $methodParamNum = $controllerMethod->getNumberOfParameters();
        if($methodParamNum == $paramNum){
            return true;
        }else{
            return false;
        }
    }
}
?>

