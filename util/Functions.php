
<?php

class Functions {

    /**
     * Input mezők szűrése a kártékony kódok ellen
     * @param string $input
     */
    public static function filter( $input ){
        $trick = array( '@<script>]*?>.*?</script>' , '@<[\/q!]*?[^<>]*?>@si' , '@<style>]*?>.*?</style>@siU' , '@<![\s\S]*?--[ \t\n\r]*>@' );
        $chars = array( '&' , '<' , '>' , '\r\n' , '|' );
        $entit = array( '&amp' , '&lt' , '&gt' , '<br/>' , '&#124;' );
        $sqlfilter = array( 'INSERT' , 'INTO' , 'SELECT' , "UPDATE" , 'DROP' , 'ALTER' );
        //$output = preg_replace( $trick, '-' , $input );
        $output = str_replace( $chars , $entit , $input );
        $output = str_replace( '<' , '' , $output );
        $output = str_replace( '>' , '' , $output );
        $output = str_replace( $sqlfilter , "!SQLCOMM!" , $output );
        return $output;
    }

    /**
     * Kódgenerátor: rcg -> randomCodeGenerator
     * @param integer $sizeOf
     */
    public static function rcg( $sizeOf=10 ){
        $code='';
        $characters='abcdefghjklmnoprstuvwxyz0123456789';
        for( $i=0 ; $i < $sizeOf ; $i++ )$code .= substr($characters, rand() % (strlen($characters)), 1);
        return $code;
    }

    /**
     * Egy adott hosszúságú generált kód
     */
    public static function getRCG( $l = 10 ){
        $code = Functions::rcg( $l );
        while( Functions::issetMark($code)!=0 ){
            $code = Functions::rcg( $l );
        }
        return $code;
    }

    /** Megnézi van-e előfordulása a MARK-nak
     * @param string $mark
     * @return int
     */
    public static function issetMark( $mark ){
        //példányosítsunk egy db-t
        $db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
        $db->exec("SET CHARACTER SET utf8");
        $db->exec("SET NAMES utf8");
        $sth = $db->prepare( "SELECT * FROM mark WHERE code=:code");
        $sth->execute(array(":code" => $mark));
        return $sth->rowCount();
    }

    function getPages($currPage,$maxPages){

        $pages = array(); //A linkek számát tartlamazó tömb

        $linkNum = 5; //Ennyi oldal mutató link legyen összesen

        if($maxPages <= $linkNum){      //Szükséges oldalak száma kisebb vagy egyenlő mint ahány linket akarunk
            for($i = 1 ; $i <= $maxPages;$i++) {
                $pages[] = $i;
            }
        }else{
            $right = 0; //Linkek jobb oldali széle
            $left = 0; //Linkek bal oldali széle
            if($currPage > round($linkNum/2) ){      //Jobbra ugyaannyit kell "menni" mint balra
                $right = $currPage + (round($linkNum/2)-1);
                $left =  $currPage - (round($linkNum/2)-1);    //Összes link - amennyit balra mentünk - 1 ( currentPage miatt);
            }else{
                $left = 1;
                $right = $currPage + ($linkNum - $currPage);


            }
            if($right > $maxPages){     ///túlmentünk, ennyi már nem is kell
                $right = $maxPages;
            }

            for($i = $left; $i<= $right;$i++){
                $pages[] = $i;
            }
        }
        return $pages;
    }
}