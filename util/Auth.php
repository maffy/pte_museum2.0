<?php

class Auth{
    /**
     * Felhasználó authentikációhoz használjuk
     */
    public static function handleLogin(){
        Session::init();
        if( Session::get('loggedIn')==false ){
            Session::destroy();
            header('location:' .URL. 'login');
            exit;
        } else {
            $params = Auth::getAll(Session::get("role_id"));
            foreach( $params as $key => $value ){
                Session::set( $key , $value );
            }
        }
    }

    public static function getAll( $id ){
        //példányosítsunk egy db-t
        $db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
        $db->exec("SET CHARACTER SET utf8");
        $db->exec("SET NAMES utf8");

        $sth = $db->prepare("SELECT * FROM role LEFT JOIN user ON user.id=role.uid WHERE user.id=:id");
        $sth->execute(array(":id"=>$id["id"]));

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public static function role( $role ){
        if( Session::get( $role )!=0 && Session::get( $role )!=1 ) header('location:' .URL. 'admin');
        else {
            if( Session::get( $role )==0 ){
                header('location:' .URL. 'admin');
            }
        }
    }
}