<?php

class Picture {
    /**
     * @param $image - a képünk
     * @param $watermark - a vízjelünk
     * @param int $opt - vízjel átlátszósága
     * @return mixed - elkészült kép
     */
    public static function containMODUL( $image , $watermark , $opt=10 ){
        $w = imagesx($image);
        $h = imagesy($image);
        $thumb = imagecreatetruecolor( $w , $h );
        imagecopyresized($thumb,$watermark,0,0,0,0,$w,$h,imagesx($watermark),imagesy($watermark));

        $image = Picture::fixMODUL($image , $thumb , 0 , 0 , $opt);
        return $image;
    }

    /**
     * @param $image - a képünk
     * @param $watermark - a vízjelünk
     * @param int $opt - vízjel átlátszósága
     * @return mixed - elkészült kép
     */
    public static function mosaicMODUL( $image , $watermark , $opt=10 ){
        //előszőr nézzük meg hogy belefér-e a képbe a vízjel legalább 2x, ha nem akkor fixmodul
        if( imagesx($watermark)*2<imagesx($image) && imagesy($watermark)*2<imagesy($image) ){
            //számoljuk ki hányszor fér el benne
            $in_row = imagesx($image)/imagesx($watermark);
            $in_col = imagesy($image)/imagesy($watermark);

            $dst_x=0;
            $dst_y=0;

            for( $i=0 ; $i<round($in_col,0) ; $i++ ){
                for( $k=0 ; $k<round($in_row,0) ; $k++ ){
                    $image = Picture::fixMODUL($image,$watermark,$dst_x,$dst_y,$opt);
                    $dst_x += imagesx($watermark); //eltolás jobbra
                }
                $dst_y += imagesy($watermark); //eltolás lefele
                $dst_x = 0;
            }
        } else {
            $image = Picture::fixMODUL($image , $watermark);
        }
        return $image;
    }

    /**
     * @param $image - ez a kép amire másoljuk
     * @param $watermark - ez a vízjel
     * @param int $x - ez az X koordináta
     * @param int $y - ez az Y koordináta
     * @param int $opt - vízjel átlátszósága
     * @return mixed - a kép
     */
    public static function fixMODUL( $image , $watermark , $x=20 , $y=20 , $opt=10 ){
		//TODO: megcsinálni hogy melyik sarokba rakja a vízjelet
        //másoljuk rá a vízjelünk a képre a megadott helyre
        $dst_im = $image; //a kép melyre másoljuk a vízjelet
        $src_im = $watermark; //vízjel
        $dst_x = $x; //eltolás jobbra
        $dst_y = $y; //eltolás lefele
        $src_x = 0; //kitolás balra (ajánlott a 0)
        $src_y = 0; //kitolás felfele (ajánlott a 0)
        $src_w = imagesx($watermark); //vízjel szélessége (nem torzítható)
        $src_h = imagesy($watermark); //vízjel magassága (nem torzítható)
        $pct = $opt; //vízjel átlátszóséga (0 átlátszó - 100 nem átlátszó)
        imagecopymerge($dst_im , $src_im , $dst_x ,$dst_y , $src_x , $src_y , $src_w , $src_h , $pct); //folyamat elvégzése
        return $image;
    }

    /**
     * Az adott id-hez készít egy DB táblát és a fájloknak az elfogadása után feltölti a táblát értékekkel
     * @param integer $id
     * @param array() $FILES - ezt nem kell átadni neki
     */
    public static function handle( $id ){
        //tároljuk az üzeneteket
        $msg = array();

        //csak akkor menjünk bele a feltöltésbe ha van is mit feltölteni
        if( count($_FILES["file"]["name"])>1 || $_FILES["file"]["name"][0]!="" ){
            $waterIm = null;

            //ha akartunk vízjelet és mi töltjük fel
            if( $_POST["waterWant"] ) {
                if( $_POST["waterOwner"] ){
                    //hozzuk barátságosabb formára a vízjelet
                    $waterIm = $_FILES["waterImage"];
                    Picture::save( $waterIm , WATER_PICTURES , 1 , Functions::getRCG() , true );
                    $waterIm = $waterIm["name"];
                } else {
                    //csak a fájlnévre van szükségünk innen kezdve
                    $waterIm = $_POST["waterSelected"];
                }
            }

            //menjünk végig a file tömbön
            for( $i=0 ; $i<count($_FILES["file"]["name"]) && isset($_FILES["file"]["name"]) ; $i++ ){

                //hozzuk szép formára az aktuális indexű fájlunk
                $file = array();
                $file["name"] = $_FILES["file"]["name"][$i];
                $file["tmp_name"] = $_FILES["file"]["tmp_name"][$i];
                $file["size"] = $_FILES["file"]["size"][$i];
                $file["type"] = $_FILES["file"]["type"][$i];
                $file["error"] = $_FILES["file"]["error"][$i];

                //példányosítsunk egy db-t
                $db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
                $db->exec("SET CHARACTER SET utf8");
                $db->exec("SET NAMES utf8");

                //ha nincs akkor vigyük be az adatbázisba
                $data = array();
                $data["code"] = Functions::getRCG();
                $data["date"] = date('Y-m-d');
                $db->insertDyn("mark", $data);

                //generljunk egy kódot
                $e_code = Functions::getRCG();

                //mentsük le a képet
                $sub = Picture::save( $file , UPLOAD_PICT_DIR , 2 , $e_code , false , $id );
                $msg = array_merge( $msg , $sub );

                if( Picture::WaterAccess($file["name"],$id) ) {
                    //generáljunk rá vízjelet ha kell
                    Picture::genWater($file, $waterIm, $_POST["waterType"], $e_code);
                }
            }//for vége

            //szabadítsuk fel a memóriát
            unset($_POST);
            return $msg;
        }
    }//handlePics vége

    /**
     * Megnézzük hogy adatbázisban hozzávan-e rendelve
     * @param $filename
     * @param $id
     * @return bool
     */
    public static function WaterAccess( $filename , $id ){
        //példányosítsunk egy db-t
        $db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
        $db->exec("SET CHARACTER SET utf8");
        $db->exec("SET NAMES utf8");

        //kérdezzük le hogy van-e már hozzá adat
        $sth = $db->prepare("SELECT mark FROM exhibit_im__$id WHERE file=:filename");
        $sth ->execute(array(":filename" => $filename));
        $data = $sth->fetch(PDO::FETCH_ASSOC);

        //ha van false, amúgy minden esetben true
        if( strlen($data["mark"])>0 && file_exists( UPLOAD_PICT_DIR.$data["mark"] ) ) return false;
        return true;
    }

    /** Elmenti az átadott objektumot a megadott utvonalra
     * @param object $file
     * @param string $path
     * @param int $sizeMB
     * @param null $newName
     * @param bool|false $water
     * @param null $id
     */
    public static function save( $file , $path  , $sizeMB = 1  , $newName=null , $water = false , $id=null ){
        //szerver üzenetekhez
        $msg = array(array());
        $msgIndex = 0;

        //példányosítsunk egy db-t
        $db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
        $db->exec("SET CHARACTER SET utf8");
        $db->exec("SET NAMES utf8");

        //feltölthető képek kiterjesztései, ha ezt módosítod, akkor lejjebb van két switch is amit módosítani kell!
        $extensions = array( "jpg","jpeg","png","gif" );

        //utvonal és fájlnév
        $target_file = $path.$file["name"];

        //fájltípus
        $imType = pathinfo($file["name"],PATHINFO_EXTENSION);

        //maximális feltölthető fájlméret
        $maxFileSize = 1048576 * $sizeMB;

        //ezt a változót 0-ra állítjuk ha valami nem stimmel
        $upLoadOk = 1;

        //megnézzük hogy létezik-e a fájl
        if( file_exists($target_file) && $upLoadOk==1 ){
            $msg[$msgIndex]["success"] = 0;
            $msg[$msgIndex++]["message"] = "Ez a fájl már létezik! <font color='orange'>".basename($file["name"])."</font>";
            $upLoadOk = 0;
        }

        //megnézzük a fájlméretet
        if( $file["size"]>$maxFileSize && $upLoadOk==1  ){
            $msg[$msgIndex]["success"] = 0;
            $msg[$msgIndex++]["message"] = "Ez a fájl túl nagy! (maximum: ".sprintf("%.2f",$maxFileSize/1024)."KB) <font color='orange'>".basename($file["name"])."</font>";
            $upLoadOk = 0;
        }

        //megvizsgáljuk a fájl kiterjesztését (csak képet engedjük)
        if( !in_array($imType,$extensions) && $upLoadOk==1  ){
            //dinamikus lista esetén
            $extensions_list= implode(", ",$extensions);
            $msg[$msgIndex]["success"] = 0;
            $msg[$msgIndex++]["message"] = "Csak a következő kiterjesztések megengedettek: ".$extensions_list."! <font color='orange'>".basename($file["name"])."</font>";
            $upLoadOk = 0;
        }

        //bármi gond -> $upLoadOk = 0, egyébként feltölt
        if( $upLoadOk == 0 ){
            $msg[$msgIndex]["success"] = 0;
            $msg[$msgIndex]["message"] = "A fájlt nem sikerült feltölteni! <font color='orange'>".basename($file["name"])."</font>";
        } else {
            if( move_uploaded_file($file["tmp_name"],$target_file) ){

                if($water){
                    //ha ez vízjel akkor összeállítjuk az adatbázisba felvitendő adatokat
                    $data = array();
                    $data["image"] = basename($file["name"]);
                    $data["date"] = date("Y-m-d");
                    $db->insertDyn( "water" ,$data);
                } else {
                    //ha ez nem vízjel akkor összeállítjuk az adatbázisba felvitendő adatokat
                    $data_im = array();
                    $data_im["file"] = $file["name"];
                    $data_im["mark"] = $newName.".".$imType;
                    $db->insertDyn('exhibit_im__'.$id ,$data_im);
                }
                $msg[$msgIndex]["success"] = 1;
                $msg[$msgIndex++]["message"] = "Sikeresen feltöltve! <font color='orange'>".basename($file["name"])."</font>";
            } else {
                $msg[$msgIndex]["success"] = 0;
                $msg[$msgIndex++]["message"] = "Valami nem sikerült a feltöltésben. <font color='orange'>".basename($file["name"])."</font>";
                unset($_POST["uploadImage"]);
            }
            //megnézzük a hibakódot
            if ( $file["error"] > 0){
                $msg[$msgIndex]["success"] = 0;
                $msg[$msgIndex]["message"] = "Error: " . $file["error"] . " <font color='orange'>".basename($file["name"])."</font>";
                /*
                0 => 'There is no error, the file uploaded with success',
                1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
                2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
                3 => 'The uploaded file was only partially uploaded',
                4 => 'No file was uploaded',
                6 => 'Missing a temporary folder',
                7 => 'Failed to write file to disk.',
                8 => 'A PHP extension stopped the file upload.',
                */
            }
        }
        return $msg;
    }//save vége


    /** Legenerálja a választott opcióhoz megfelelően a vízjelet
     * @param object $file
     * @param string $waterName
     * @param int $want
     * @param string $e_code
     */
    static function genWater( $file , $waterName , $want=3 , $e_code ){

        //ebbe fogjuk belefésülni a vízjelet, ha van <- vízjeles kép lesz
        $image = null;
        $watermark = null;

        //vízjel utvonal
        $watermark_path = WATER_PICTURES.$waterName;

        //fájltípus
        $imType = pathinfo($file["name"],PATHINFO_EXTENSION);

        //kiterjesztésünknek megfelelő függvénnyel hozzuk létre a képünk objektumát
        switch ($imType) {
            case 'jpg': {$image = imagecreatefromjpeg(UPLOAD_PICT_DIR.$file["name"]);break;}
            case 'jpeg': {$image = imagecreatefromjpeg(UPLOAD_PICT_DIR.$file["name"]);break;}
            case 'png': {$image = imagecreatefrompng(UPLOAD_PICT_DIR.$file["name"]);break;}
            case 'gif': {$image = imagecreatefromgif(UPLOAD_PICT_DIR.$file["name"]);break;}
        }

        //nézzük meg létezik-e a watermakrunk
        if (file_exists($watermark_path)) {
            $waterFileType = pathinfo($watermark_path, PATHINFO_EXTENSION);
            //létrehozzuk a formátumnak megfelelő objektumot
            switch ($waterFileType) {
                case 'jpg' : {$watermark = imagecreatefromjpeg($watermark_path);break;}
                case 'jpeg' : {$watermark = imagecreatefromjpeg($watermark_path);break;}
                case 'png' : {$watermark = imagecreatefrompng($watermark_path);break;}
                case 'gif' : {$watermark = imagecreatefromgif($watermark_path);break;}
            }
        }

        //ha van vízjel
        if( $want ) {
            switch( $want ){
                case 1 : { Picture::mosaicMODUL( $image , $watermark , $_POST["opacity"] ); break;}
                case 2 : { Picture::containMODUL( $image , $watermark , $_POST["opacity"] ); break;}
                case 3 : { Picture::fixMODUL( $image , $watermark , $_POST["opacity"] ); break;}
            }
        }

        //objektumból kimenő fájl-t előállítjuk a megfelelő (~50/100) minőséggel
        switch ($imType) {
            case 'jpg': {imagejpeg($image, UPLOAD_PICT_DIR . $e_code.".".$imType, 50);break;}
            case 'jpeg': {imagejpeg($image, UPLOAD_PICT_DIR . $e_code.".".$imType, 50);break;}
            case 'png': {imagepng($image, UPLOAD_PICT_DIR . $e_code.".".$imType, 5);break;}
            case 'gif': {imagegif($image, UPLOAD_PICT_DIR . $e_code.".".$imType);break;}
        }
        //memória felszabadítása
        imagedestroy($image);
    } //genWater vége
}