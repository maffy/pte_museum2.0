<?php

class Log {

    public static function setLog( $data ){
        //példányosotsunk egy db-t
        $db = new Database(DB_TYPE,DB_HOST,DB_NAME,DB_USER,DB_PASS);
        $db->exec("SET CHARACTER SET utf8");
        $db->exec("SET NAMES utf8");

        $sth = $db->prepare("INSERT INTO log (date,time,addr,data) VALUES (NOW(),NOW(),:addr,:data)");
        $sth->execute(array(
            ":data"=>$data,
            ":addr"=>$_SERVER["REMOTE_ADDR"]
        ));
    }

}