<?php
//[ MÓDOSÍTHATÓ ]
	//FIGYELEM: ezt a .htaccess fájlban is állitsuk be általában csak a "/" kell
    //Alapértelmezett weboldalcím (/ jel a végén!) például: http://example.com/
    define('URL' , 'http://localhost/git/pte_museum2.0/');

//[ CSAK ÓVATOSAN ]
    //Museum kép tárolásához utvonal - csak a legelső fájlfeltöltés elött, az adatbázisban nem teljes utvonalat határozzuk meg
    define('UPLOAD_PICT_DIR','public/image/exhibition/');

    //Ezek bármikor megváltoztathatók, de figyljünk oda, hogy a megadott utvonalon létezzen a fájl
    define('FILE_DEFAULT_WR','public/image/original/default.png');      //alapértelmezett WR: WRONG
    define('FILE_DEFAULT_NE','public/image/original/not-exist.png');    //nincs ilyen kép/nem létezik NE: NOT EXIST
    define('WATERMARK_PICT','public/image/original/watermark.png');     //vízjel
    define('WATER_PICTURES','public/image/watermarks/');       //vízjelek mappája

    //Időzóna beállítása
    date_default_timezone_set('UTC');

    //DATABASE
    define('DB_TYPE','mysql');      //adatbátis típusa
    define('DB_HOST','localhost');  //adatbázist futtató host/ipcím
    define('DB_USER','root');       //adatbázis felhasználó
    define('DB_PASS','');           //adatbázis felhasználójához tartozó jelszó
    define('DB_NAME','museum');     //adatbázis neve

    //Speciális kódolás md5-ön felül
    define('HASH_KEY','fjriwew44334____43kfdd');

//[ NEM MÓDOSÍTHATÓ ]
    //Itt vannak a Core fájlok
    define('LIBS','libs/');
