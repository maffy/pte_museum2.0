-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2015. Nov 26. 08:12
-- Kiszolgáló verziója: 5.6.26
-- PHP verzió: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `museum`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `idarticle` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `abstract` text COLLATE utf8mb4_hungarian_ci,
  `location` varchar(100) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `dateAdded` datetime NOT NULL,
  `isFile` int(11) NOT NULL DEFAULT '0',
  `download` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `article`
--

INSERT INTO `article` (`idarticle`, `title`, `abstract`, `location`, `author`, `dateAdded`, `isFile`, `download`) VALUES
(14, 'cikk', 'cikk', 'public/data/564762075c9145.73842571.pdf', 'jh', '2015-11-14 04:11:07', 1, NULL),
(15, 'Ius Comparatum - Global Studies in Comparative Law', 'As globalization proceeds, the significance of the comparative approach in legal scholarship increases. The IACL / AIDC with almost 700 members is the major universal organization promoting comparative research in law and organizing congresses with hundreds of participants in all parts of the world. ', 'http://link.springer.com/bookseries/11943', 'VA', '2015-11-16 12:11:57', 0, NULL),
(16, 'Cim1', 'abstract1', 'location1', 'szerzo1', '2015-11-16 04:11:44', 0, NULL),
(21, 'Article with tags', 'abstract', 'valid url', 'szero', '2015-11-18 04:11:31', 0, NULL),
(24, 'Vadonatuj function', 'abstarctsada', 'public/data/565485b9889692.72976971.docx', 'szerzo', '2015-11-24 00:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `article_tag`
--

CREATE TABLE IF NOT EXISTS `article_tag` (
  `articleid` int(11) NOT NULL,
  `tag` varchar(30) COLLATE utf8mb4_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `article_tag`
--

INSERT INTO `article_tag` (`articleid`, `tag`) VALUES
(24, 'macska');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `idcontent` int(11) NOT NULL,
  `page` varchar(30) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci,
  `content` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `content`
--

INSERT INTO `content` (`idcontent`, `page`, `title`, `content`) VALUES
(1, 'home', ' Informatika tÃ¶rtÃ©neti mÃºzeum ', 'NÃ©zzen kÃ¶rbe!');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `exhibit`
--

CREATE TABLE IF NOT EXISTS `exhibit` (
  `id` int(11) NOT NULL,
  `head` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `auth` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `file` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `disc` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `date` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `exhibit`
--

INSERT INTO `exhibit` (`id`, `head`, `auth`, `file`, `disc`, `date`, `modified`) VALUES
(33, 'sadaa sdasd', 'sd aasd asd asdas ', 'evidence-of-water-found-on-mars-5652760466817024.2-hp.gif', '<p>&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da&nbsp;asd asd as da</p>', '2015-11-07', '2015-11-07');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `exhibit_im__`
--

CREATE TABLE IF NOT EXISTS `exhibit_im__` (
  `id` int(11) NOT NULL,
  `file` text COLLATE utf8_hungarian_ci NOT NULL,
  `mark` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `exhibit_im__`
--

INSERT INTO `exhibit_im__` (`id`, `file`, `mark`) VALUES
(1, '11715517_10206837988446891_590123724_o.jpg', 'xszu1p12vj.jpg');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(20) NOT NULL,
  `username` text COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `realname` text COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `regip` varchar(16) COLLATE utf8_hungarian_ci NOT NULL,
  `regtime` date NOT NULL,
  `lastmodify` date NOT NULL,
  `lastin` date NOT NULL,
  `ban` int(2) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `realname`, `email`, `regip`, `regtime`, `lastmodify`, `lastin`, `ban`, `rank`) VALUES
(18, 'owner', '1d1285ffaeae379c3d3caedeef5bfdd8', 'Tulajdonos', 'tulajdonos@tulaj.com', '192.160.0.0', '0000-00-00', '0000-00-00', '0000-00-00', 0, 100),
(19, 'user2', 'b9c76c01b4dd1e5df83572af58b0c4ab', 'teljes', 'email', '192.0.1.1', '2015-11-11', '2015-11-11', '2015-11-11', 0, 0);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`idarticle`);

--
-- A tábla indexei `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`idcontent`);

--
-- A tábla indexei `exhibit`
--
ALTER TABLE `exhibit`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `exhibit_im__`
--
ALTER TABLE `exhibit_im__`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `article`
--
ALTER TABLE `article`
  MODIFY `idarticle` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT a táblához `content`
--
ALTER TABLE `content`
  MODIFY `idcontent` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `exhibit`
--
ALTER TABLE `exhibit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT a táblához `exhibit_im__`
--
ALTER TABLE `exhibit_im__`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
